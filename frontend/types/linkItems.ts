export interface NavItems {
    label: string;
    href: string;   
}