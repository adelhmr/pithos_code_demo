import nextJest from 'next/jest';
const createJestConfig = nextJest({ dir: '.' });

const config: Object = {
  transform: {
    '^.+\\.[jt]sx?$': ['babel-jest'],
    '^.+\\.tsx?$': 'ts-jest',
  },
  // Ajoutez ici vos configurations personnalisées pour Jest
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  testEnvironment: 'jest-environment-jsdom',
  clearMocks: true,
  moduleDirectories: ['node_modules', 'app/[locale]'],
};

module.exports = createJestConfig(config);
