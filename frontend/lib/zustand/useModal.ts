
import { create } from 'zustand';
// Modal Store
interface useModal {
  isOpen: boolean;
  onOpen: () => void;
  onClose: () => void;
}

const TermsAndConditionModal = create<useModal>((set) => ({
  isOpen: false,
  onOpen: () => set({ isOpen: true }),
  onClose: () => set({ isOpen: false }),
}));

export default TermsAndConditionModal;
