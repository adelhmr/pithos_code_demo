import React from 'react'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import Navbar from '@/app/[locale]/components/Navbar'

describe('Navbar page', () => {
  it('Should render properly', () => {
    render(<Navbar />)

    // const header = screen.getByRole('heading');

    // expect(header).toHaveTextContent()

    // Utilisez l'attribut 'locale' pour sélectionner la balise <Link> contenant le texte de la langue spécifique
    const englishLink = screen.getByText('English')
    const frenchLink = screen.getByText('Francais')
    const arabicLink = screen.getByText('Arabe')

    // Vérifiez que les liens de langue sont présents dans le composant Navbar
    expect(englishLink).toBeInTheDocument()
    expect(frenchLink).toBeInTheDocument()
    expect(arabicLink).toBeInTheDocument()
  })
})
