'use client'
// import axios from 'axios'
import { AiFillGithub } from 'react-icons/ai'
import { FcGoogle } from 'react-icons/fc'
import { useCallback, useState } from 'react'
import { FieldValues, SubmitHandler, useForm } from 'react-hook-form'
import useModal from 'lib/zustand/modalStore'
import Heading from '../shared/heading'
import Input from '../shared/Input'
import Button from '../shared/Button'
import Modal from '../shared/Modal'


const TermsModal = () => {
  const termsModal = useModal()
  const [isLoading, setIsLoading] = useState(false)
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      name: '',
      email: '',
      password: '',
    },
  })
  const onSubmit: SubmitHandler<FieldValues> = (data) => {
    setIsLoading(true)
    // axios
    //   .post(`/api/register`, data)
    //   .then(() => {
    //     registerModal.onClose()
    //   })
    //   .catch((error) => {
    //     toast.error('Something went wrong')
    //   })
    //   .finally(() => {
    //     setIsLoading(false)
    //   })
  }
  const bodyContent = (
    <div className='flex flex-col gap-4'>
      <Heading title='Welcome to Airbnb' subtitle='Join our network!' center />
      <Input
        id='name'
        label='Name'
        disabled={isLoading}
        register={register}
        required
        errors={errors}
      />
      <Input
        id='email'
        label='Email'
        type='email'
        disabled={isLoading}
        register={register}
        required
        errors={errors}
      />
      <Input
        id='pass'
        label='Password'
        type='password'
        disabled={isLoading}
        register={register}
        required
        errors={errors}
      />
    </div>
  )
  const footerContent = (
    <div className='flex flex-col gap-4 mt-3'>
      <hr />
      <Button outline label='Continue with Google' icon={FcGoogle} onClick={() => {}} />
      <Button outline label='Continue with Github' icon={AiFillGithub} onClick={() => {}} />
      <div className='text-neutral-500 text-center mt-4 font-light'>
        <div className='flex flex-row items-center gap-2'>
          <div>Already have an account?</div>
          <div>
            <div
              onClick={termsModal.onClose}
              className='text-neutral-800 cursor-pointer hover:underline'
            >
              Login
            </div>
          </div>
        </div>
      </div>
    </div>
  )
  return (
    <Modal
      disabled={isLoading}
      isOpen={termsModal.isOpen}
      title='Register'
      actionLabel='Continue'
      onClose={termsModal.onClose}
      onSubmit={handleSubmit(onSubmit)}
      body={bodyContent}
      footer={footerContent}
    />
  )
}

export default TermsModal
