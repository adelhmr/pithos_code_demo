import { useTranslations } from 'next-intl'
import Image from 'next/image';

const carousel = () => {
    const t = useTranslations('homepage');
    return (
        <>
            <section className="hero bg-cover bg-center h-screen relative bg-[url('/openOffice.jpg')] p-0">
                <section className="bg-gray-800/70 dark:bg-gray-900/70 h-full w-full flex items-center align-middle">
                    <div className="grid w-full [height: 480px] px-4 py-8 mx-auto xl:gap-6 xl:py-16 xl:grid-cols-12 ">
                        <div className="mx-10 xl:col-span-8">
                            <h1 className=" w-full mb-12 text-2xl text-primary-500  font-extrabold tracking-tight leading-none md:text-5xl xl:text-6xl dark:text-white">
                                {t('heroSectionTitle')}
                            </h1>
                            <p className=" w-full mb-12 font-light text-white xl:mb-8 md:text-lg lg:text-xl dark:text-gray-400">
                            We don't just build software, we craft strategic solutions that streamline your operations, optimize your marketing, and fuel your growth. Partner with us and experience the power of tech that propels you forward.
                            </p>
                            <a href="#" className="inline-flex items-center justify-center px-5 py-3 mr-3 text-base font-medium text-center text-white rounded-lg bg-orange-500 hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 dark:focus:ring-primary-900">
                            Speak to Sales
                                <svg className="w-5 h-5 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>
                            </a>
                        </div>
                        <div className="hidden xl:mt-0 xl:col-span-4 xl:flex mr-40">
                            <img src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/hero/phone-mockup.png" alt="mockup"/>
                            {/* {<Image 
                                src='/herosectionIMG.png'
                                width={500}
                                height={500}
                                alt=''
                            />} */}
                        </div>
                    </div>
                </section>

            </section>
        </>
    )
}

export default carousel