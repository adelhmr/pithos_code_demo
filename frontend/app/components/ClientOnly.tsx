//This File is to avoid hydration Error 'error when we reload many time and start clicking on the page'
'use client'
import { useEffect, useState } from 'react'
interface ClientOnlyProps {
  children: React.ReactNode
}
const ClientOnly: React.FC<ClientOnlyProps> = ({ children }) => {
  const [hasMounted, setHasMounted] = useState(false)
  useEffect(() => {
    setHasMounted(true)
  }, [])
  if (!hasMounted) {
    return null
  }
  return <>{children}</>
}

export default ClientOnly
