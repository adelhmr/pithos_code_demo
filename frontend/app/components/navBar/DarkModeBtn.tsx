'use client'
import { useTheme } from 'next-themes'
import { useEffect, useState } from 'react'
import { FaRegSun, FaMoon} from "react-icons/fa";
export default function DarkModeBtn(): React.ReactNode {
  const { theme, setTheme, systemTheme } = useTheme()
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    setMounted(true)
  }, [])

  if (!mounted) {
    return null
  }
  const currentTheme = theme === 'system' ? systemTheme : theme
  return (
    <>
      {currentTheme === 'dark' ? (
        <FaRegSun
          className='h-6 w-6 cursor-pointer text-orange-500'
          onClick={() => {
            setTheme('Light')
          }}
        />
      ) : (
        <FaMoon
          className='h-6 w-6 cursor-pointer text-orange-500'
          onClick={() => {
            setTheme('dark')
          }}
        />
      )}
    </>
  )
}
