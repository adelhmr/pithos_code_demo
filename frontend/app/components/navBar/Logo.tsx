'user component'
import PGT from '../../../public/Logo.svg'
import Image from 'next/image'

const Logo = (): React.ReactNode => {
  return (
    <>
      <Image src={PGT} alt='Pithos Global Technology' width={80} height={30} />
    </>
  )
}

export default Logo
