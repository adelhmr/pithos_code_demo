'use client'

import { useState, useCallback, useRef, useEffect } from 'react'
import { AiOutlineMenu } from 'react-icons/ai'
import Avatar from '../shared/Avatar'
import MenuItem from './MenuItem'
import useSinInModal from 'lib/zustand/useSignInModal'
import useRegisterModal from 'lib/zustand/useRegisterModal'
import { useOnClickOutside } from 'app/custom-hooks/useOnClickOutside'
const UserMenu = () => {
  const regModal = useRegisterModal()
  const signModal = useSinInModal()
  //TODO: Use SignIn Modal should be customized for signIn not
  
  const [isOpen, setIsOpen] = useState(false )
  const toggleOpen = useCallback(() => {
    setIsOpen((value) => !value)
  }, [])
  const navRef = useRef<HTMLDivElement | null>(null)
    useOnClickOutside(navRef, (e: any) => setIsOpen(false))
    useEffect(() => {
        const handler = (e: KeyboardEvent) => {
            if (e.key === 'Escape') {
                setIsOpen(false)
            }
            document.addEventListener('keydown', handler)
            return () => {
                document.removeEventListener('keydown', handler)
            }
        }
    })
  
  return (
    <div className='relative'>
      <div className='flex flex-row items-center gap-3'>

        <div
          onClick={toggleOpen}
          className='p-4 md:py-1 md:px-2 border-[1px]  border-orange-500 flex flex-row items-center gap-3 rounded-full cursor-pointer hover:shadow-md transition'>
          <div className="text-orange-500">
          <AiOutlineMenu />
          </div>
          <div className='hidden md:block'>
            <Avatar />
          </div>
        </div>
      </div>
      {isOpen && (
        <div
        ref={navRef}
          className='absolute rounded-xl shadow-md w-[120px]  bg-gray-200/80  dark:bg-gray-800/80  text-orange-500 overflow-hidden right-0 top-12 text-sm'
        >
          <div className='flex flex-col cursor-pointer'>
            <>
              <MenuItem onClick={signModal.onOpen} label='Login' />
              <MenuItem onClick={regModal.onOpen} label='Sign up' />
            </>
          </div>
        </div>
      )}
    </div>
  )
}

export default UserMenu
