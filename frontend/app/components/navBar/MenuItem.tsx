'use client'
interface MenuItemProps {
  onClick: () => void
  label: string
}

const MenuItem: React.FC<MenuItemProps> = ({ onClick, label }) => {
  return (
    <div
      onClick={onClick}
      className='
                px-4
                py-4
                hover:bg-gray-300
                dark:hover:bg-gray-700
                transition
                font-semibold
            '
    >
      {label}
    </div>
  )
}

export default MenuItem
