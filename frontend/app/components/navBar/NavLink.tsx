'use client'

import Link from 'next/link';
import { usePathname } from 'next/navigation';

interface NavLinkProps {
  href: string;
  label: string;
  isActive: boolean;
}

const NavLink: React.FC<NavLinkProps> = ({ href, label, isActive }) => {
    const pathName = usePathname()

    return (
    <li>
      <Link href={href} passHref>
        <a className={`block py-2 pl-3 pr-4 rounded ${isActive ? 'bg-gray-100 text-orange-500' : 'text-gray-900 hover:bg-gray-100'} md:bg-transparent md:p-0`}>
          {label}
        </a>
      </Link>
    </li>
  );
};

export default NavLink;