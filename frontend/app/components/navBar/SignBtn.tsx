'use client'
import { useEffect, useState } from 'react'
import { FaRegUser } from "react-icons/fa";
export default function DarkModeBtn(): React.ReactNode {
  
  return (
    <>
        <FaRegUser
          className='h-6 w-6 cursor-pointer text-orange-500'
          onClick={() =>  {}}
        />
    </>
  )
}
