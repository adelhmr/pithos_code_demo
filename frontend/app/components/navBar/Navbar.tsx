'use client'

import { usePathname } from 'next/navigation';
import React, { useEffect, useState } from 'react'
import DarkModelBtn from './DarkModeBtn'
import Logo from './Logo'
import { useLocale, useTranslations } from 'next-intl'
import SelectDropDown from '../shared/selectDropDown'
import { NavItems } from 'types/linkItems'
import Link from 'next/link';
import UserMenu from './UserMenu';

export default function NavBar(): React.ReactNode {
  const t = useTranslations('navBar')
  const navItems: NavItems[] = [
    { label: t('home'), href: '/' },
    { label: t('about'), href: '/about' },
    { label: t('services'), href: '/services' },
    { label: t('contact'), href: '/contact' },
  ];
  const locale = useLocale()
  const path = usePathname()
  let initScrollState: boolean

  if (window.scrollY === 0) {
    initScrollState = true
  } else {
    initScrollState = false
  }
  const [navStyle, setNavStyle] = useState(initScrollState)
  const handleScroll = () => {
    if (window.scrollY >= 40) {
      setNavStyle(false);
    } else {
      setNavStyle(true);
    }
  };
  useEffect(() => {

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.addEventListener('scroll', handleScroll);
    };
  }, []);

  const getActivePath = (locale: String, href: String) => {
    let activepath = '/' + locale
    if (href.length != 1) {
      activepath = `/${locale}${href}`
    }
    return activepath
  }

  return (
    <>

      <nav className={
        navStyle ?
          `bg-transparent fixed top-0 left-0 right-0 z-10 border-gray-200  dark:bg-transparent m-0  ` :
          `bg-white fixed top-0 left-0 right-0 z-10 shadow-xl dark:bg-gray-900 m-0 `
      }>

        <div className='max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4'>
          <a href='/' className='flex items-center'>
            <Logo />
          </a>
          <div className='flex items-center md:order-2'>
            <SelectDropDown />
            <div className='flex items-center space-x-2 mx-2'>
              <DarkModelBtn />
            </div>
            <div className='flex items-center space-x-2 mx-2'>
              <UserMenu/>
            </div>
          </div>
          <div
            className='items-center justify-between hidden w-full md:flex md:w-auto md:order-1'
            id='navbar-language'
          >
            <ul className="flex space-x-4">
              {navItems.map((item, index) => (

                <li key={index}>
                  <Link href={item.href} className={`${path === getActivePath(locale, item.href)
                    ? ' text-orange-500 hover:text-primary-600'
                    : ` text-${path === `/${locale}` && navStyle ? 'white' : 'gray-900'} dark:text-white hover:text-primary-600`
                    } block py-2 pl-3 pr-4 bg-transparent rounded hover:bg-gray-100 md:hover:bg-transparent  md:p-0  md:dark:hover:bg-transparent dark:border-gray-700`}
                  >
                    {item.label}
                  </Link>
                </li>
              ))}
            </ul>

          </div>
        </div>
      </nav>
    </>
  )
}
