'use client'

import { IconType } from 'react-icons'

interface ButtonProps {
  label: string
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void
  disabled?: boolean
  outline?: boolean
  small?: boolean
  icon?: IconType
}
const Button: React.FC<ButtonProps> = ({
  label,
  onClick,
  disabled,
  outline,
  small,
  icon: Icon, //This is an alias of Icon to be used in condition we have an Icon
}) => {
  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`
                relative
                disabled:opacity-70
                disabled:cursor-not-allowed
                rounded-lg
                hover:opacity-80
                transition
                w-full
                focus:ring-2 focus:outline-none focus:ring-orange-500 
                ${outline ? 'bg-white hover:bg-gray-100 dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700' : 'bg-orange-500'}
                ${outline ? 'border-gray-900' : 'border-orange-500'}
                ${outline ? 'text-gray-900' : 'text-white'}
                ${small ? 'py-1' : 'py-3'}
                ${small ? 'text-sm' : 'text-md'}
                ${small ? 'font-light' : 'font-semibold'}
                ${small ? 'border-[1px]' : 'border-[2px]'}
                
            `}
    >
      {Icon && (
        <Icon
          size={18}
          className='
                        absolute
                        left-4
                        top-3
                    '
        />
      )}
      {label}
    </button>
  )
}

export default Button
