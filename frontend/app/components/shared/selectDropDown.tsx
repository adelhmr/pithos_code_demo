'use client'
import { useLocale } from 'next-intl';
import { useRouter } from 'next/navigation';
import { useState, useTransition, useMemo } from 'react';
import { MdKeyboardArrowDown } from 'react-icons/md';


export default function LocalSwitcher() {
    const localeOptions: {
        [key: string]: string; 
      } = useMemo(() => ({
        en: 'English',
        fr: 'Français',
        // Add more locales here
      }), []);
  const [isOpen, setIsOpen] = useState<null | boolean>(null);
  const [isPending, startTransition] = useTransition();
  const router = useRouter();
  const localActive = useLocale();

  const handleLocaleChange = (nextLocale: string) => {
    startTransition(() => {

      router.replace(`/${nextLocale}`);
    });
  };

  return (
    <div className="relative inline-block">
      <button
        id="dropdownDefaultButton"
        onClick={() => setIsOpen(!isOpen)}
        data-dropdown-toggle="dropdown"
        className="text-white bg-orange-500 hover:bg-primary-500 focus:ring-0.5 focus:outline-none focus:ring-orange-500 font-medium rounded-lg text-sm px-5 py-2.5 inline-flex items-center dark:bg-orange-500 dark:hover:bg-orange-500 dark:focus:ring-primary-500 relative"
        type="button"
        aria-label="Select Locale" // Added aria-label for accessibility
      >
        {localeOptions[localActive]}
        <MdKeyboardArrowDown className={`
          text-base
          text-white
          transition-all
          ${isOpen ? 'rotate-180' : 'rotate-0'}`} />
      </button>

      {/* Dropdown menu */}
      {isOpen && (
        <div
          id="dropdown"
          className="z-50 bg-gray-300/70 shadow-xl divide-y  rounded-lg  w-full dark:bg-gray-700/90 absolute mt-2"
        >
          <ul className="py-2 text-sm divide-gray-800 text-gray-900 dark:text-gray-200 text-center max-content inline-block w-full" aria-labelledby="dropdownDefaultButton">
          {Object.keys(localeOptions).map((localeCode) => (
              <li key={localeCode}>
                <button
                  className="block p-2 w-full hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                  onClick={() => handleLocaleChange(localeCode)}
                >
                  {localeOptions[localeCode]}
                </button>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
