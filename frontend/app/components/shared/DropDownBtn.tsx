'use client'
import { useOnClickOutside } from 'app/custom-hooks/useOnClickOutside';
import Link from 'next/link';
import React, { ReactNode, useEffect, useRef, useState } from 'react';
import { MdKeyboardArrowDown } from 'react-icons/md';

interface Option {
    label: string;
    value: string;
}

interface DropdownButtonProps {
    options: Option[],
    headerPlaceholder?: ReactNode | string,
    children?: ReactNode,// Allow any JSX content as children
    onChange: (newValue: string) => void,
    value: ReactNode | string
}

const DropdownButton: React.FC<DropdownButtonProps> = ({ options, onChange, headerPlaceholder, children, value }) => {
    const [isOpen, setIsOpen] = useState<null | boolean>(null)
    const [selectedOption, setSelectedOption] = useState<Option | null>(null);
    const navRef = useRef<HTMLDivElement | null>(null)
    useOnClickOutside(navRef, (e: any) => setIsOpen(null))
    useEffect(() => {
        const handler = (e: KeyboardEvent) => {
            if (e.key === 'Escape') {
                setIsOpen(null)
            }
            document.addEventListener('keydown', handler)
            return () => {
                document.removeEventListener('keydown', handler)
            }
        }
    })
    return (
        <div className="relative inline-block text-left" >
            <div>
                <button
                    type="button"
                    tabIndex={0}
                    className="text-dark   focus:outline-none  font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center bg-transparent focus:ring-none"
                    onClick={() => setIsOpen(!isOpen)}
                >
                    {value ? value : headerPlaceholder}
                    <MdKeyboardArrowDown className={`text-base text-orange-500 transition-all ${isOpen ? 'rotate-180' : 'rotate-0'}`} />


                </button>
            </div>

            {isOpen && (
                <div ref={navRef} className="z-10 absolute bg-white divide-y divide-gray-100 rounded-lg shadow w-full dark:bg-gray-700/80" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                    {
                        options.map((p) =>
                            <div onClick={() => {
                                onChange(p.label)
                                setIsOpen(false)
                            }}
                                className={`flex py-2 px-3 cursor-pointer ${p.label === value ? 'bg-gray-200' : 'bg-transparent'}`}>
                                <Link href={`/${p.value}`} locale={p.value} >
                                    {p.label}
                                </Link>

                            </div>
                        )

                    }
                </div>
            )}
        </div>
    );
};

export default DropdownButton;
