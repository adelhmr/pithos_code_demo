'use client'
//import axios from 'axios'
// import { AiFillGithub } from 'react-icons/ai'
import { FcGoogle } from 'react-icons/fc'
import { useCallback, useState } from 'react'
import { FieldValues, SubmitHandler, useForm } from 'react-hook-form'
import Modal from './Modal'
import useRegisterModal from 'lib/zustand/useRegisterModal'
import Heading from './heading'
import Input from './Input'
import Button from './Button'

const RegisterModal = () => {
  const registerModal = useRegisterModal()
  const [isLoading, setIsLoading] = useState(false)
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldValues>({
    defaultValues: {
      name: '',
      email: '',
      password: '',
    },
  })
  const onSubmit: SubmitHandler<FieldValues> = (data) => {
    setIsLoading(true)
    // axios
    //   .post(`/api/register`, data)
    //   .then(() => {
    //     registerModal.onClose()
    //   })
    //   .catch((error) => {
    //     toast.error('Something went wrong')
    //   })
    //   .finally(() => {
    //     setIsLoading(false)
    //   })
  }
  const bodyContent = (
    <div className='flex flex-col gap-4'>
      <Heading
        title='Welcome to Pithos Global Technology'
        subtitle='Join our network!'
        center />
      <Input
        id='name'
        label='Name'
        disabled={isLoading}
        register={register}
        required
        errors={errors}
      />
      <Input
        id='email'
        label='Email'
        type='email'
        disabled={isLoading}
        register={register}
        required
        errors={errors}
      />
      <Input
        id='pass'
        label='Password'
        type='password'
        disabled={isLoading}
        register={register}
        required
        errors={errors}
      />
    </div>
  )
  const footerContent = (
    <div className='flex flex-col gap-4 mt-3'>
      <hr />
      <Button outline label='Continue with Google' icon={FcGoogle} onClick={() => {}} />
      {/* <Button outline label='Continue with Github' icon={AiFillGithub} onClick={() => {}} /> */}
      <div className='text-neutral-500 text-center mt-4 font-light'>
        <div className='flex flex-row items-center gap-2'>
          <div className='dark:text-orange-500'>Already have an account?</div>
          <div>
            <div
              onClick={registerModal.onClose}
              className='text-neutral-800 cursor-pointer hover:underline dark:text-orange-500 font-semibold'
            >
              Login
            </div>
          </div>
        </div>
      </div>
    </div>
  )
  return (
    <Modal
      disabled={isLoading}
      isOpen={registerModal.isOpen}
      title='Register now!'
      actionLabel='Continue'
      onClose={registerModal.onClose}
      onSubmit={handleSubmit(onSubmit)}
      body={bodyContent}
      footer={footerContent}
    />
  )
}

export default RegisterModal
