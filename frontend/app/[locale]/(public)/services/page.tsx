'user component'

import ServicesList from "../../../components/services/servicesList"

const Services = (): React.ReactNode => {
  return (
    <>
    <div className='px-6 h-screen  w-full mb-16'>
      <div className="mx-auto max-w-screen-md text-center mb-8 lg:mb-12 mt-40">
          <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">Our services</h2>
          <p className="mb-5 font-light text-gray-500 sm:text-xl dark:text-gray-400">We provide cutting edge technology</p>
      </div>
      <div className="mx-auto max-w-screen-xl text-center mb-8 lg:mb-12 mt-25">
          <ServicesList/>
      </div>
    </div>

    </>
  )
}

export default Services
