'user component'

const Blog = (): React.ReactNode => {
  return (
    <>
    <div className="h-screen pt-20">
    <h1 className='text-center text-4xl'>
      Blog Page
      <div>
        <div className='flex justify-between p-4 lg:p-8'>
          <div className='flex text-start space-x-2 text-lg'>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quia itaque ab tempore iure
            placeat iusto, quisquam reiciendis consequuntur repellat blanditiis, saepe laboriosam
            atque praesentium accusamus molestiae hic nemo temporibus, repudiandae illo minima
            vitae. Veniam tenetur ducimus quo nesciunt culpa at eum hic doloremque soluta. Eveniet
            quae suscipit beatae at quas!{' '}
          </div>
        </div>
      </div>
    </h1>
    </div>
    
    </>
  )
}

export default Blog
