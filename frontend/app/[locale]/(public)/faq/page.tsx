import FaqList from "../../../components/faq/faqList"
import Container from "../../../components/shared/Container"
import SearchinputWithoptions from "../../../components/shared/SearchinputWithoptions"

const FAQ = (): React.ReactNode => {
  return <>
    <div className="h-screen mt-20 mb-40">
      <div className="flex flex-col items-center ">
        <h1 className="text-center text-3xl mb-6">FAQ</h1>
        <SearchinputWithoptions />
      </div>
      <Container>
        <FaqList />
      </Container>
    </div>

  </>
}

export default FAQ
