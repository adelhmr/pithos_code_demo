'use client'
// import DarkModelBtn from './components/navBar/DarkModeBtn'
import Link from 'next/link'
import HeroSection from '../../../components/home/heroSection'
import Services from '../../../components/home/services'
import NewsLetter from '../../../components/home/newsLetter'
import Pricing from '../../../components/home/pricing'
import PartnerWithUs from '../../../components/home/partnerWithUs'
// import { useTranslations } from 'next-intl'

export default function Home(): React.ReactNode {
  // nconst t = useTranslations('Index')

  return (
    <>
      <div>
        <div className=' p-0'>
          <h1 className='text-4xl lg:text-6xl'>
            <Link href='/'></Link>
          </h1>
          <div className='hero-section'>
            <HeroSection/>
          </div>
          <div className="services md:my-15 my-auto">
           <Services/>
          </div>
          <div className="why-to-choose-us md:my-15 my-auto">
            <PartnerWithUs/>
          </div>
          <div className="pricing md:my-15 my-auto">
            <Pricing/>
          </div>
          <div className="news-letter md:my-15 my-auto">
            <NewsLetter/>
          </div>
        </div>
      </div>
    </>
  )
}
