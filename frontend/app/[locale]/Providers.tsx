'use client'
import React from 'react'
import { ThemeProvider } from 'next-themes'
import NavBar from '../components/navBar/Navbar'
import Footer from '../components/footer/footer'
import ClientOnly from '../components/ClientOnly'
import RegisterModal from '../components/shared/RegisterModal'
function Providers({ children }: { children: React.ReactNode }): React.ReactNode {
  return (
    <ThemeProvider attribute='class' defaultTheme='system' enableSystem>
      <RegisterModal />
      <ClientOnly>
        <NavBar />
      </ClientOnly>

      {children}
      <ClientOnly>

        <Footer />
      </ClientOnly>
    </ThemeProvider>
  )
}

export default Providers
