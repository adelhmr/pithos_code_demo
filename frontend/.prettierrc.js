module.exports = {
    // Use single quotes instead of double quotes.
    singleQuote: true,
    // Add trailing commas where valid in ES5 (objects, arrays, etc.).
    trailingComma: 'es5',
    // Use 2 spaces for indentation.
    tabWidth: 2,
    // Wrap lines at 100 characters.
    printWidth: 100,
    // Disallow console.log statements.
    noConsole: 'error',
    // Enforce the use of === and !== instead of == and !=.
    eqeqeq: 'error',
    // Enforce consistent spacing around keywords.
    keywordSpacing: 'error',
    // Apply these options to all *.tsx files.
    overrides: [{
        files: '*.tsx',
        options: {
            // Use the TypeScript parser.
            parser: 'typescript',
            // Wrap lines at 100 characters.
            printWidth: 100,
            // Use single quotes instead of double quotes.
            singleQuote: true,
            // Add trailing commas where valid in ES5 (objects, arrays, etc.).
            trailingComma: 'es5',
            // Disallow console.log statements.
            noConsole: 'error',
            // Enforce the use of === and !== instead of == and !=.
            eqeqeq: 'error',
            // Enforce consistent spacing around keywords.
            keywordSpacing: 'error',
            // Enforces two spaces for indentation.
            indent: ['error', 2],
            // Do not require semicolons.
            semi: false,
            // Enforce consistent spacing around unary operators.
            spaceUnaryOps: 'error',
            // Use single quotes in JSX.
            jsxSingleQuote: true,
            // Enforce consistent spacing inside curly braces in JSX.
            jsxCurlySpacing: ['error', 'always'],
            // Enforce consistent placement of closing brackets in JSX tags.
            'react/jsx-closing-bracket-location': ['error', 'tag-aligned'],
            // Enforce consistent spacing inside curly braces in JSX, except for props on the same line as the opening tag.
            'react/jsx-curly-spacing': ['error', { when: 'never', children: true }],
            // Enforce a maximum of one prop per line in JSX.
            'react/jsx-max-props-per-line': ['error', { when: 'always', maximum: 1 }],
            // Enforces PascalCase for user-defined JSX components
            'react/jsx-pascal-case': 'error',
        },
    }, ],
};