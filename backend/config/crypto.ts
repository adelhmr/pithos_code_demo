import crypto from 'crypto';
export const cryptoParams = async () => {
  // Define Algorithm
  const cryptAlgo = 'aes-256-cbc';
  // generate 16 bytes of random data
  const initVector = await crypto.randomBytes(16);

  // secret key generate 32 bytes of random data
  const securityKey = await crypto.randomBytes(32);
  const cryptoParams = { cryptAlgo, initVector, securityKey };
  return cryptoParams;
};
