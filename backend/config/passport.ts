import { createRSAKeyPair } from '../src/utils/authenticationUtils';
import fs from 'fs';
import { isFileAlreadyExist } from '../src/utils/fsUtils';

export const passportParams = (): {
  jwtSecretKey: string;
  jwtSession: {
    session: boolean;
  };
  jwtExpiration: string;
} => {
  if (!isFileAlreadyExist('./config/rsa', 'privateKey.pem')) {
    createRSAKeyPair();
  }
  const jwtSecretKey = fs.readFileSync('./config/rsa/privateKey.pem', 'utf8');
  const jwtSession = {
    session: false,
  };
  const jwtExpiration = '1h';

  const passportParams = {
    jwtSecretKey,
    jwtSession,
    jwtExpiration,
  };
  return passportParams;
};
