import morgan, { type StreamOptions } from 'morgan'
import fs from 'fs'

// Create a custom stream for logging
const accessLogStream = fs.createWriteStream('logs/access.log', { flags: 'a' })

const stream: StreamOptions = {
  write: (message) => {
    accessLogStream.write(message + '\n')
  },
}

const logger = morgan('combined', { stream })

export default logger
