import fs from 'fs'
import path from 'path'

export const logError = (error: Error): void => {
  if (error instanceof Error) {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    const errorMessage = `${new Date().toISOString()} -  ${error.stack}\n`
    const errorLogPath = path.join(__dirname, 'error.log')

    fs.appendFile(errorLogPath, errorMessage, (err) => {
      if (err) {
        if (err.code === 'ENOENT') {
          fs.writeFile(errorLogPath, errorMessage, (writeErr) => {
            if (writeErr) {
              console.error('Failed to create the error.log file:', writeErr)
            }
          })
        } else {
          console.error('Failed to log error:', err)
        }
      }
    })
  } else {
    console.error('Invalid error object:', error)
  }
}
