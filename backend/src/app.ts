import express, { type Application } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';
import fs from 'fs';
import path from 'path';
import { logError } from '../logs/errors';
import logger from '../logs/logger';
import cookieParser from 'cookie-parser';
import useragent from 'express-useragent';
// import { passport } from '../config/passport-config'
// import { errorHandler } from './middleware/errorHandler'
// import { notFoundHandler } from './middleware/notFoundHandler'
import userRoutes from './routes/userRoutes';
import userQueryRoutes from './routes/userQueryRoutes';
import ShortNews from './routes/shortNewsRoutes';
import visitorRoutes from './routes/visitorRoutes';
// Import other routes as needed

// Create Express application
const app: Application = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(cookieParser());
app.use(useragent.express());
// Add headers
app.use(helmet());
// logging system

// Set up the Morgan logger middleware
app.use(logger);

// Set up error logging middleware
// Middleware pour gérer les erreursp
app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
  try {
    // Some code that might throw an error
    throw new Error('this is an error');
  } catch (err) {
    if (err instanceof Error) {
      logError(err); // Explicitly specify the type with type assertion
    }
  }
  next(err);
});

// app.use(passport.initialize());
// Routes
app.use('/api/v1', userRoutes);
app.use('/api/v1', userQueryRoutes);
app.use('/api/v1/content/short-news', ShortNews);
// Register other routes as needed
app.use('/api/v2', visitorRoutes);

// Error handling middleware
// app.use(errorHandler)

// Not found middleware
// app.use(notFoundHandler)

export default app;
