import { type Document } from 'mongoose';
import { type ConnectionData } from './ConnectionDataInterfaces';

export interface IUser extends Document {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  failedLoginAttempts: number;
  lastFailedLogin: Date | null;
  role: string;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
  connection: ConnectionData;
  comparePassword: (candidatePassword: string) => Promise<boolean>;
}

export interface AuthenticateUser {
  id: string;
  email: string;
  role: string;
}
