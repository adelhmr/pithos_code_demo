export interface ConnectionData {
  ipAddress: string[];
  os: string[];
  deviceType: string[];
  lastLoginAt: Date | null;
  timeStamp: Date | null;
  browser: string[];
}
