import { type Schema } from 'mongoose';
import { type JwtPayload } from 'jsonwebtoken';
export interface Usertoken {
  userId: Schema.Types.ObjectId;
  token: string;
  createdAt: Date;
}
export interface DecodedTokenPayload extends JwtPayload {
  id: string;
  email: string;
  iat: number;
  exp: number;
}
