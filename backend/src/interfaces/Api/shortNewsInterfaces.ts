import { type Schema, type Document } from 'mongoose';

export interface shortNews extends Document {
  title: string;

  content: string;

  creator_id: Schema.Types.ObjectId;

  creationDate: Date;

  status: string;

  toArchiveOn: Date;

  ArchivingDate: Date;
}
