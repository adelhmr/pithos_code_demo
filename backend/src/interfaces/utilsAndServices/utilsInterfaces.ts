export interface EmailOptions {
  senderEmail: string;
  receiverEmails: string[];
  subject?: string;
  text?: string;
  html?: string;
  link?: string;
}
