import app from './app'
import envConfig from '../config/env/index'
import connectDB from '../config/dbConnection/dbconnector'
import { logError } from '../logs/errors'
const PORT = envConfig.port !== null && envConfig.port !== undefined ? envConfig.port : 4500

connectDB()
  .then(() => {
    // Start the server
    app.listen(PORT, () => {
      console.log(`
🚀 Server ready at: http://localhost:${PORT}
⭐️ Welcome to Pithos Global Technology server`)
    })
  })
  .catch((error) => {
    console.error('🚨 Failed to connect to MongoDB:', error)
    try {
      // Some code that might throw an error
      throw new Error('🚨 Failed to connect to MongoDB:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
