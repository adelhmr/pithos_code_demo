import nodemailer from 'nodemailer'
import { type EmailOptions } from '../interfaces/utilsAndServices/utilsInterfaces'
import envConfig from '../../config/env/index'
const sendgridApiKey = envConfig.sg_api_key

export const transporter = nodemailer.createTransport({
  host: 'smtp.sendgrid.net',
  port: 587,
  auth: {
    user: 'apikey',
    pass: sendgridApiKey,
  },
})

export async function sendEmail(options: EmailOptions): Promise<void> {
  const { senderEmail, receiverEmails, subject, text, html } = options

  const mailOptions = {
    from: senderEmail,
    to: receiverEmails,
    subject,
    text,
    html,
  }

  transporter.sendMail(mailOptions, (err, res) => {
    if (err != null) {
      console.log(err)
    } else {
      console.log('The email was sent successfully')
    }
  })
}

export async function sendPasswordResetEmail(
  senderEmail: string,
  receiverEmails: string[],
  link: string
): Promise<void> {
  const subject = 'Magic Link for Password Reset'
  const text = `Click the following link to reset your password: ${link}`

  await sendEmail({ senderEmail, receiverEmails, subject, text })
}

export async function sendNewsletter(
  senderEmail: string,
  receiverEmails: string[],
  text: string
): Promise<void> {
  const subject = 'Newsletter'
  await sendEmail({ senderEmail, receiverEmails, subject, text })
}

export async function sendBlogAlert(
  senderEmail: string,
  receiverEmails: string[],
  text: string
): Promise<void> {
  const subject = 'New Blog Post'
  await sendEmail({ senderEmail, receiverEmails, subject, text })
}
