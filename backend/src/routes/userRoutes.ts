import express from 'express'
import { type Request, type Response } from 'express'
import { login, registerInternalUser, resetIUser } from '../controllers/userController'
import { logError } from '../../logs/errors'
const router = express.Router()

router.post('/iUser/register', (req: Request, res: Response): void => {
  registerInternalUser(req, res).catch((error) => {
    try {
      // Some code that might throw an error
      throw new Error('Error in registerInternalUser:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
    console.error('Error in registerInternalUser:', error)
  })
})

router.post('/login', (req: Request, res: Response): void => {
  login(req, res).catch((error) => {
    console.error('Error in login:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in login:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})

router.put('/iUser/reset-user', (req: Request, res: Response): void => {
  resetIUser(req, res).catch((error) => {
    console.error('Error in resetIUser:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in resetIUser:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})

export default router
