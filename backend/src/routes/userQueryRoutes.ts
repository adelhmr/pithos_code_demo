import express from 'express'
import { type Request, type Response } from 'express'
import {
  getAllUsers,
  profile,
  updateIUserById,
  deleteIUserById,
  deleteAllUsers,
} from '../controllers/userController'
import { authenticate } from '../middleware/authenticationMiddleware'
import { logError } from '../../logs/errors'
import { AuthenticateUser, type IUser } from '../interfaces/Api/IUserInterfaces'
const router = express.Router()

router.use(authenticate)

router.get('/iUser', (req: Request, res: Response): void => {
  getAllUsers(req, res).catch((error) => {
    console.error('Error in getAllUsers:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in getAllUsers:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})

router.delete('/iUser/:id', (req: Request, res: Response): void => {
  deleteIUserById(req, res).catch((error) => {
    console.error('Error in deleteIUserById:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in deleteIUserById:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})

router.delete('/iUser/', (req: Request, res: Response): void => {
  deleteAllUsers(req, res).catch((error) => {
    console.error('Error in deleteAllUsers:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in deleteAllUsers:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})

router.get('/profile', profile)

router.get('/protected', authenticate, (req, res) => {
  // Vous pouvez accéder à l'utilisateur authentifié via req.user
  if (req.user) {
    const user = req.user as IUser
    res.json({ message: 'You are authorized!', user })
  }
})

router.put('/iUser/:id', (req: Request, res: Response): void => {
  updateIUserById(req, res).catch((error) => {
    console.error('Error in updateIUserById:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})

export default router
