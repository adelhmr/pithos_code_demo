import express from 'express'
import { type Request, type Response } from 'express'

import {
  getAllVisitors,
  login,
  profile,
  updateVisitorById,
  registerVisitor,
  deleteVisitorById,
  deleteAllVisitors,
  resetVisitor,
} from '../controllers/visitorController'
import { authenticate } from '../middleware/authenticationMiddleware'
const router = express.Router()

router.post('/Visitor/register', (req: Request, res: Response): void => {
  registerVisitor(req, res).catch((error) => {
    console.error('Error in registerInternalUser:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})

router.get('/Visitor', (req: Request, res: Response): void => {
  getAllVisitors(req, res).catch((error) => {
    console.error('Error in getAllUsers:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})

router.delete('/Visitor/:id', (req: Request, res: Response): void => {
  deleteVisitorById(req, res).catch((error) => {
    console.error('Error in deleteIUserById:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})

router.delete('/Visitor/', (req: Request, res: Response): void => {
  deleteAllVisitors(req, res).catch((error) => {
    console.error('Error in deleteAllUsers:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})

router.post('/login', (req: Request, res: Response): void => {
  login(req, res).catch((error) => {
    console.error('Error in login:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})
router.get('/profile', authenticate, profile)
router.put('/Visitor/reset-user', (req: Request, res: Response): void => {
  resetVisitor(req, res).catch((error) => {
    console.error('Error in resetIUser:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})

router.put('/Visitor/:id', (req: Request, res: Response): void => {
  updateVisitorById(req, res).catch((error) => {
    console.error('Error in updateIUserById:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})

export default router
