import express from 'express'
import { type Request, type Response } from 'express'
import { authenticate } from '../middleware/authenticationMiddleware'
import { logError } from '../../logs/errors'
import {
  deleteAllShortNews,
  deleteShortNewsById,
  getAllShortNews,
  getlatestShortNews,
  createShortNews,
  updateShortNewsById,
} from '../controllers/shortNewsController'
const router = express.Router()

router.use(authenticate)
router.post('/create', (req: Request, res: Response): void => {
  createShortNews(req, res).catch((error) => {
    try {
      // Some code that might throw an error
      throw new Error('Error in createShortNews:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
    console.error('Error in createShortNews:', error)
  })
})

router.get('/', (req: Request, res: Response): void => {
  getAllShortNews(req, res).catch((error) => {
    console.error('Error in getAllShortNews:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in getAllShortNews:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})

router.get('/latestShortNews', (req: Request, res: Response): void => {
  getlatestShortNews(req, res).catch((error) => {
    console.error('Error in getlatestShortNews:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in getlatestShortNews:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})
router.put('/ShortNews/:id', (req: Request, res: Response): void => {
  updateShortNewsById(req, res).catch((error) => {
    console.error('Error in updateShortNewsById:', error)
    res.status(500).json({ error: 'Internal server error' })
  })
})
router.delete('/ShortNews/:id', (req: Request, res: Response): void => {
  deleteShortNewsById(req, res).catch((error) => {
    console.error('Error in deleteIUserById:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in deleteShortNewsById:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})
router.delete('/ShortNews/', (req: Request, res: Response): void => {
  deleteAllShortNews(req, res).catch((error) => {
    console.error('Error in deleteAllShortNews:', error)
    try {
      // Some code that might throw an error
      throw new Error('Error in deleteAllShortNews:')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  })
})

export default router
