import nodemailer from 'nodemailer'
import { EmailOptions } from '../interfaces/utilsAndServices/utilsInterfaces'
// Helper function to send the magic link email
export const sendEmail = async (
  senderEmail: string,
  receiverEmails: [string],
  content: string,
  link?: string
): Promise<void> => {
  // Configure nodemailer to send emails
  const transporter = nodemailer.createTransport({
    host: 'smtp.sendgrid.net',
    port: 587,
    auth: {
      user: 'apikey',
      pass: process.env.SEND_GRID_API_KEY,
    },
  })

  // Compose the email message
  const mailOptions = {
    from: senderEmail,
    to: receiverEmails,
    subject: 'Magic Link for Password Reset',
    text: `Click the following link to reset your password: ${link ?? ''}`,
    html: `<p>Click the following link to reset your password: <a href="${link ?? ''}">${
      link ?? ''
    }</a></p>`,
  }

  // Send the email
  await transporter.sendMail(mailOptions)
}
