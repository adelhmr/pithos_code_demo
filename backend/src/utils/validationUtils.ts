// Example validation utility functions
export const isValidEmail = (email: string): boolean => {
  // Implement email validation logic
  return true
}

export const isValidPassword = (password: string): boolean => {
  const minLength = 10
  const maxLength = 20
  const regexUppercase = /[A-Z]/
  const regexLowercase = /[a-z]/
  const regexNumber = /[0-9]/
  const regexSpecialCharacter = /[!@#$%^&*]/

  if (password.length < minLength) {
    return false
  }
  // max length check
  if (password.length > maxLength) {
    return false
  }

  if (
    !(
      regexUppercase.test(password) &&
      regexLowercase.test(password) &&
      regexNumber.test(password) &&
      regexSpecialCharacter.test(password)
    )
  ) {
    return false
  }

  return true
}
