import UserToken from '../models/userToken'
import jwt from 'jsonwebtoken'
import { passportParams } from '../../config/passport'
import { type DecodedTokenPayload } from '../interfaces/Api/UserTokenInterfaces'
import { decryptData } from './secUtils'
import { cryptoParams } from '../../config/crypto'

const authConfig = passportParams()
const verifyRefreshToken = async (userid: string): Promise<DecodedTokenPayload | null> => {
  try {
    const userToken = await UserToken.findOne({ userId: userid })

    if (!userToken) {
      return null
    }

    const refreshdecrypt = await decryptData(userToken.token)
    const decodedtoken = jwt.verify(refreshdecrypt, authConfig.jwtSecretKey) as DecodedTokenPayload
    if (!decodedtoken) {
      return null
    }
    return decodedtoken
  } catch (error) {
    console.error(error)
    return null
  }
}

export default verifyRefreshToken
