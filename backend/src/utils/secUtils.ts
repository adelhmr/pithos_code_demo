import bcrypt from 'bcrypt'
import crypto from 'crypto'
import jwt from 'jsonwebtoken'
import { cryptoParams } from '../../config/crypto'

const cryptParams = cryptoParams()
// Initial Hashed password Generate function
const saltRound = 12
export const generateInitialIUPassword = async (): Promise<string> => {
  const length: number = Math.floor(Math.random() * 5) + 13 // Random length between 13 and 17
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+'
  let password = ''

  for (let i = 0; i < length; i++) {
    const randomIndex: number = Math.floor(Math.random() * chars.length)
    password += chars.charAt(randomIndex)
  }
  const salt = await bcrypt.genSalt(saltRound)
  const hashedPassword = await bcrypt.hash(password, salt)
  return hashedPassword
}

// Hash any needed data with the needed Salt Round
export const hashData = async (data: string, sltRnd: number): Promise<string> => {
  const hashedData = await bcrypt.hash(data, sltRnd)
  return hashedData
}

// Encrypt data method

// export const encryptData = async (
//   data: string,
//   algorithm: string,
//   securityKey: Buffer,
//   initVector: Buffer
// ): Promise<string> => {
//   // create cipher
//   const cipher = crypto.createCipheriv(algorithm, securityKey, initVector);

//   let encryptedData = cipher.update(data, 'utf-8', 'hex');
//   encryptedData += cipher.final('hex');
//   return encryptedData;
// };
export const encryptData = async (data: string): Promise<string> => {
  // create cipher
  const cipher = crypto.createCipheriv(
    (await cryptParams).cryptAlgo,
    (await cryptParams).securityKey,
    (await cryptParams).initVector
  )

  let encryptedData = cipher.update(data, 'utf-8', 'hex')
  encryptedData += cipher.final('hex')
  return encryptedData
}

// Decrypt data method

export const decryptData = async (data: string): Promise<string> => {
  try {
    // the decipher function
    const decipher = crypto.createDecipheriv(
      (await cryptParams).cryptAlgo,
      (await cryptParams).securityKey,
      (await cryptParams).initVector
    )

    let decryptedData = decipher.update(data, 'hex', 'utf-8')

    decryptedData += decipher.final('utf8')

    return decryptedData
  } catch (error) {
    console.error('Decryption Error:', error)
    throw new Error('Decryption failed')
  }
}

/*
   needed for encrypting and decrypting
    // Define Algorithm
    const cryptAlgo = "aes-256-cbc"
    // generate 16 bytes of random data
    const initVector = await crypto.randomBytes(16);

    // secret key generate 32 bytes of random data
    const securityKey = await crypto.randomBytes(32);



    to be checked if we need to encrypt method should return an object in .env files
*/

// Generate JWT Token method
export const generateToken = (payload: object, key: string, expiry: string): string | undefined => {
  try {
    const token = jwt.sign({ ...payload }, key, { expiresIn: expiry })
    return token
  } catch (err) {
    console.log(err)
  }
}
