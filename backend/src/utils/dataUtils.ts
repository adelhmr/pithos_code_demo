import { type Request } from 'express'
import { type Usertoken } from '../interfaces/Api/UserTokenInterfaces'

export const getIpAdress = (req: Request): string => {
  const ip = req.ip!
  console.log(ip)
  return ip
}
export const getLoginDateTime = (): Date => {
  const timeStamp = Date.now()
  return new Date(timeStamp)
}

export const getLastLogin = (userToken: Usertoken | null): Date | null => {
  try {
    if (!userToken || userToken == null) {
      return new Date(Date.now())
    }
    const lastLogin = userToken.createdAt
    return lastLogin
  } catch (error) {
    console.error(error)
    return null
  }
}
export const getDeviceType = (req: Request): string => {
  const device = req.useragent?.isMobile ? 'Mobile' : 'Desktop'
  console.log(device)
  return device
}
export const getBrowser = (req: Request): string => {
  const browser = req.useragent?.browser ?? 'Unknown'
  console.log(browser)
  return browser
}

export const getOstype = (req: Request): string => {
  const os = req.useragent?.os ?? 'Unknown'
  console.log(os)
  return os
}
