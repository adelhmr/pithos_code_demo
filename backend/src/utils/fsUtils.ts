import fs from 'fs'
import path from 'path'
import { Error } from 'mongoose'

export const createFile = async (
  targetDirPath: string,
  fileName: string,
  fileContent?: string | undefined
): Promise<string | undefined> => {
  if (isFileAlreadyExist(targetDirPath, fileName)) {
    const existingFileMessage = 'A file with the same name is already existing!'
    console.log(existingFileMessage)
    return existingFileMessage
  } else {
    try {
      // Create the directory if it does not exist
      fs.mkdirSync(targetDirPath, { recursive: true })

      // Create the directory if it does not exist
      const filePath = path.join(targetDirPath, fileName)
      fs.writeFileSync(filePath, fileContent ?? 'Some text here')
      console.log('File created successfully.')
    } catch (err) {
      console.error(err)
    }

    // how to call it to create a file into config directory "createFile('./config/rsa','priv.txt')"
  }
}

export const isFileAlreadyExist = (directoryPath: string, fileName: string): boolean => {
  const filePath = `${directoryPath}/${fileName}`
  if (!fs.existsSync(filePath)) {
    return false
  } else {
    return true
  }
}
