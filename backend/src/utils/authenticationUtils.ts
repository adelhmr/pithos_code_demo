import { generateKeyPair, generateKeyPairSync } from 'crypto'
import { createFile } from './fsUtils'
import jwt from 'jsonwebtoken'
import { v4 as uuidv4 } from 'uuid'

export const createRSAKeyPair = (): void => {
  // Generate RSA key_pair (publicKey, privateKey)
  const keyPair = generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem',
    },
    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'pem',
      cipher: 'aes-256-cbc',
      passphrase: 'top secret',
    },
  })
  // Create publicKey file
  createFile('./config/rsa', 'publicKey.pem', keyPair.publicKey).catch((error) => {
    // Handle the error here
    console.error(error)
  })

  // Create privateKey file
  createFile('./config/rsa', 'privateKey.pem', keyPair.privateKey).catch((error) => {
    // Handle the error here
    console.error(error)
  })
}

export const generateMagicLink = (): string => {
  const secretKey =
    process.env.JWT_SECRET_KEY !== null && process.env.JWT_SECRET_KEY !== undefined
      ? process.env.JWT_SECRET_KEY
      : uuidv4()

  const token = jwt.sign({}, secretKey, { expiresIn: '1 hour' })
  const baseUrl = process.env.BASE_URL

  return `${baseUrl ?? ''}/reset-password?token=${token ?? ''}`
}
