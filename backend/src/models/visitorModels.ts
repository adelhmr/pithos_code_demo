import mongoose, { Schema } from 'mongoose'
import bcrypt from 'bcrypt'
import { type VisitorInterface } from '../interfaces/Api/VisitorInterfaces'

const visitorSchema = new Schema<VisitorInterface>({
  firstName: {
    type: String,
    required: true,
    unique: false,
  },
  lastName: {
    type: String,
    required: true,
    unique: false,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  failedLoginAttempts: {
    type: Number,
    default: 0,
  },
  lastFailedLogin: {
    type: Date,
    default: null,
  },
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user',
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  connection: {
    ipAddress: { type: [String], default: [] },
    os: { type: [String], default: [] },
    deviceType: { type: [String], default: [] },
    lastLoginAt: { type: Date, default: null },
  },
})

// Add the comparePassword method to the visitorSchema
visitorSchema.methods.comparePassword = async function (
  candidatePassword: string
): Promise<boolean> {
  const visitor = this as VisitorInterface
  return await bcrypt.compare(candidatePassword, visitor.password)
}

const Visitor = mongoose.model<VisitorInterface>('Visitor', visitorSchema)

export default Visitor
