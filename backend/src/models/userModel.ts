import mongoose, { Schema, type Document } from 'mongoose'
import bcrypt from 'bcrypt'
import { type IUser } from '../interfaces/Api/IUserInterfaces'

const userSchema = new Schema<IUser>({
  firstName: {
    type: String,
    required: true,
    unique: false,
  },
  lastName: {
    type: String,
    required: true,
    unique: false,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  failedLoginAttempts: {
    type: Number,
    default: 0,
  },
  lastFailedLogin: {
    type: Date,
    default: null,
  },
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user',
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  connection: {
    ipAddress: { type: [String], default: [] },
    os: { type: [String], default: [] },
    deviceType: { type: [String], default: [] },
    lastLoginAt: { type: Date, default: null },
    timeStamp: { type: Date, default: null },
    browser: { type: [String], default: [] },
  },
})

// Add the comparePassword method to the userSchema
userSchema.methods.comparePassword = async function (candidatePassword: string): Promise<boolean> {
  const user = this as IUser
  return await bcrypt.compare(candidatePassword, user.password)
}

const User = mongoose.model<IUser>('User', userSchema)

export default User
