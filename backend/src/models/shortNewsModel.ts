import mongoose, { Schema } from 'mongoose'
import { type shortNews } from '../interfaces/Api/shortNewsInterfaces'

const shortNewsSchema = new Schema<shortNews>({
  title: {
    type: String,
    required: true,
    unique: false,
  },
  content: {
    type: String,
    required: true,
    maxlength: 140,
  },
  creator_id: { type: Schema.Types.ObjectId, required: true },

  creationDate: {
    type: Date,
    default: Date.now,
  },
  status: {
    type: String,
    enum: ['draft', 'published', 'under review', 'archived'],
    default: 'draft',
  },

  toArchiveOn: { type: Date, default: null },

  ArchivingDate: { type: Date, default: null },
})

const ShortNews = mongoose.model<shortNews>('ShortNews', shortNewsSchema)

export default ShortNews
