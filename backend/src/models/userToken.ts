import mongoose, { Schema } from 'mongoose'
import { type Usertoken } from '../interfaces/Api/UserTokenInterfaces'

const userTokenSchema = new Schema<Usertoken>({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  token: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    expires: 86400, // 1 day
  },
})

const UserToken = mongoose.model<Usertoken>('UserToken', userTokenSchema)

export default UserToken
