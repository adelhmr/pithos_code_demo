import { isValidPassword } from './../utils/validationUtils'
import { type Request, type Response } from 'express'
import User from '../models/userModel'
import UserToken from '../models/userToken'
import jwt from 'jsonwebtoken'
import { passportParams } from '../../config/passport'
import { sendEmail, transporter } from '../services/emailService'
import { encryptData, generateInitialIUPassword, generateToken, hashData } from '../utils/secUtils'
import { generateMagicLink } from '../utils/authenticationUtils'
import { EmailOptions } from '../interfaces/utilsAndServices/utilsInterfaces'
import { logError } from '../../logs/errors'
import {
  getBrowser,
  getDeviceType,
  getIpAdress,
  getLastLogin,
  getLoginDateTime,
  getOstype,
} from '../utils/dataUtils'

const authConfig = passportParams()
try {
  // Some code that might throw an error
  throw new Error('this is an error')
} catch (err) {
  if (err instanceof Error) {
    logError(err) // Explicitly specify the type with type assertion
  }
}

// Create New User
export const registerInternalUser = async (req: Request, res: Response): Promise<void> => {
  try {
    // Generate a random password for the user
    const password = await generateInitialIUPassword()
    // Check if the email already exists
    const existingUser = await User.findOne({ email: req.body.email })
    if (existingUser != null) {
      // 409 Status for data conflict
      res.status(409).json({ message: 'This email address is already in use' })
      try {
        // Some code that might throw an error
        throw new Error('This email address is already in use')
      } catch (error) {
        if (error instanceof Error) {
          logError(error) // Explicitly specify the type with type assertion
        }
      }
      return
    }

    // Create a new admin user
    const user = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password, // Hash the password securely
      isActive: false, // Set the user as inactive initially
    })

    // Save the user to the database
    await user.save()

    const id = user._id
    const token = generateToken({ id, email: user.email }, process.env.JWT_SECRET_KEY ?? '', '3h')

    // Set the token as a cookie with a 15-minute expiry
    res.cookie('resetToken', token, {
      httpOnly: true,
      secure: false, // Use 'true' if using HTTPS
      sameSite: 'lax',
      maxAge: 900000, // Expiration time in milliseconds (e.g., 15 minutes)
    })
    // Create the magic link URL with the token
    const magicLink = generateMagicLink()
    // call send reset password mail
    const options = {
      senderEmail: 'contact@pithostechnology.com',
      receiverEmails: [user.email],
      subject: 'Reset password',
      text: 'This link is valid only for 1 hour',
      html: `<div>
        <p>To reset your password click to </p>
        <a href=${magicLink}>
         follow the link
        </a>
      </div>`,
    }

    await sendEmail(options)

    res.json({
      message: 'Internal user created successfully. Check your email for the magic link.',
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: 'internal server error',
    })
    try {
      // Some code that might throw an error
      throw new Error('internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
  await Promise.resolve()
}

// Login User
export const login = async (req: Request, res: Response): Promise<void> => {
  try {
    const { email, password } = req.body

    // Find the user in the database based on the provided email
    const user = await User.findOne({ email })

    // If the user is not found or the password is incorrect, return an error response
    if (user == null || !user.comparePassword(password)) {
      res.status(401).json({ error: 'Invalid email or password' })
      try {
        // Some code that might throw an error
        throw new Error('Invalid email or password')
      } catch (error) {
        if (error instanceof Error) {
          logError(error) // Explicitly specify the type with type assertion
        }
      }

      return
    }

    // Generate a JWT token
    const accesstoken = jwt.sign({ id: user.id, email: user.email }, authConfig.jwtSecretKey, {
      expiresIn: '5m',
    })
    const refreshtoken = jwt.sign({ id: user.id, email: user.email }, authConfig.jwtSecretKey, {
      expiresIn: '1d',
    })

    /* crypte refresh token */

    const refreshcrypt = await encryptData(refreshtoken)
    const userToken = await UserToken.findOne({ userId: user._id })
    /* ConnectionData */
    const ip = getIpAdress(req)
    const os = getOstype(req)
    const device = getDeviceType(req)
    const lastLoginAt = getLastLogin(userToken)
    const timeStamp = getLoginDateTime()
    const browser = getBrowser(req)
    const connection = user.connection
    connection.ipAddress.push(await encryptData(ip))
    connection.os.push(await encryptData(os))
    connection.deviceType.push(await encryptData(device))
    connection.lastLoginAt = lastLoginAt
    connection.timeStamp = timeStamp
    connection.browser.push(await encryptData(browser))
    user.connection = connection
    await user.save()
    if (userToken) {
      await userToken.deleteOne()
    }
    const newusertoken = new UserToken({ userId: user._id, token: refreshcrypt })
    await newusertoken.save()
    res.cookie('accesstoken', accesstoken, {
      maxAge: 300000, // 5min
      httpOnly: true,
      secure: false, // Use 'true' if using HTTPS
      sameSite: 'lax',
    })

    res.status(200).json({ accesstoken, connection })
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
    try {
      // Some code that might throw an error
      throw new Error('Internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}

export const profile = (req: Request, res: Response): void => {
  try {
    // Access the authenticated user from the request object
    const user = req.body.email

    // Return the user profile data
    res.status(200).json({ user })
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
    try {
      // Some code that might throw an error
      throw new Error('Internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}
// Get All users data
export const getAllUsers = async (req: Request, res: Response): Promise<void> => {
  try {
    const users = await User.find()
    res.json(users)
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
    try {
      // Some code that might throw an error
      throw new Error('Internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}

// Get user By Id

// Update user By Id
export const updateIUserById = async (req: Request, res: Response): Promise<void> => {
  const uId = req.params.id
  try {
    const user = await User.findByIdAndUpdate(
      uId,
      { ...req.body, updatedAt: new Date() },
      { new: true }
    )
    if (user == null) {
      res.status(401).send('No such user')
      try {
        // Some code that might throw an error
        throw new Error('No such user')
      } catch (error) {
        if (error instanceof Error) {
          logError(error) // Explicitly specify the type with type assertion
        }
      }

      return
    }

    res.json(user)
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
    try {
      // Some code that might throw an error
      throw new Error('Internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}
// Delete user By Id

export const deleteIUserById = async (req: Request, res: Response): Promise<void> => {
  const uId = req.params.id
  try {
    const user = await User.findByIdAndDelete(uId)
    if (user == null) {
      res.status(401).send('No such user')
      try {
        // Some code that might throw an error
        throw new Error('No such user')
      } catch (error) {
        if (error instanceof Error) {
          logError(error) // Explicitly specify the type with type assertion
        }
      }
      return
    }
    res.send('User deleted successfully')
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
    try {
      // Some code that might throw an error
      throw new Error('Internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}

// Delete All users
export const deleteAllUsers = async (req: Request, res: Response): Promise<void> => {
  try {
    await User.deleteMany({})
    res.send('All users deleted successfully')
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
    try {
      // Some code that might throw an error
      throw new Error('Internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}

export const resetIUser = async (req: Request, res: Response): Promise<void> => {
  try {
    const { email, password } = req.body

    if (!isValidPassword(password)) {
      res
        .status(401)
        .send('Password must include uppercase letters, lowercase letters, and numbers')
      return
    }
    const hashedPassword = await hashData(password, 10)

    const foundUser = await User.findOne({ email })
    if (!foundUser) {
      res.status(401).send('User not found')
      try {
        // Some code that might throw an error
        throw new Error('User not found')
      } catch (error) {
        if (error instanceof Error) {
          logError(error) // Explicitly specify the type with type assertion
        }
      }
      return
    }
    foundUser.password = hashedPassword
    foundUser.updatedAt = new Date()
    await foundUser.save()
    res.redirect('/api/v1/login')
  } catch (error) {
    res.status(500).json({ error })
    try {
      // Some code that might throw an error
      throw new Error()
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}
