/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { type Request, type Response } from 'express'
import ShortNews from '../models/shortNewsModel'
import { type IUser } from '../interfaces/Api/IUserInterfaces'
import { logError } from '../../logs/errors'
export const createShortNews = async (req: Request, res: Response): Promise<void> => {
  try {
    const existingShortNews = await ShortNews.findOne({ title: req.body.title })
    if (existingShortNews) {
      res.status(409).json({ message: 'This ShortNews is already existe' })
    }
    const user = req.user as IUser
    let { status } = req.body
    // Validate status against enum values or other validation logic
    if (!status) {
      status = 'draft'
    } else if (!['draft', 'published', 'under review', 'archived'].includes(status)) {
      res.status(400).json({ error: 'Invalid status value' })
      return
    }
    if (req.body.content.length > 140) {
      res.status(400).json({ error: 'Content length exceeds maximum limit' })
      return
    }
    const shortNews = new ShortNews({
      title: req.body.title,
      content: req.body.content,
      creator_id: user.id,
      status,
    })
    await shortNews.save()
    res.json({ shortNews })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: 'internal server error',
    })
  }
}

export const getAllShortNews = async (req: Request, res: Response): Promise<void> => {
  try {
    const shortNews = await ShortNews.find()
    res.json(shortNews)
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: 'internal server error',
    })
  }
}

export const getlatestShortNews = async (req: Request, res: Response): Promise<void> => {
  try {
    const XShortNews = req.query.xShortNews ?? 5
    const latestShortNews = await ShortNews.find()
      .sort({ creationDate: -1 })
      .limit(Number(XShortNews))
    res.json(latestShortNews)
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: 'internal server error',
    })
  }
}

export const updateShortNewsById = async (req: Request, res: Response): Promise<void> => {
  const shortNewsId = req.params.id
  try {
    const shortNews = await ShortNews.findByIdAndUpdate(
      shortNewsId,
      { ...req.body, updatedAt: new Date() },
      { new: true }
    )
    if (shortNews == null) {
      res.status(401).send('No such shortNews')
      try {
        // Some code that might throw an error
        throw new Error('No such user')
      } catch (error) {
        if (error instanceof Error) {
          logError(error) // Explicitly specify the type with type assertion
        }
      }

      return
    }

    res.json(shortNews)
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: 'internal server error',
    })
  }
}

export const deleteShortNewsById = async (req: Request, res: Response): Promise<void> => {
  const shortNewsId = req.params.id
  try {
    const shortNews = await ShortNews.findByIdAndDelete(shortNewsId)
    if (shortNews == null) {
      res.status(401).send('No such shortNews')
      try {
        // Some code that might throw an error
        throw new Error('No such shortNews')
      } catch (error) {
        if (error instanceof Error) {
          logError(error) // Explicitly specify the type with type assertion
        }
      }
      return
    }
    res.send('shortNews deleted successfully')
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: 'internal server error',
    })
  }
}

export const deleteAllShortNews = async (req: Request, res: Response): Promise<void> => {
  try {
    await ShortNews.deleteMany({})
    res.send('All ShortNews deleted successfully')
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
    try {
      // Some code that might throw an error
      throw new Error('Internal server error')
    } catch (error) {
      if (error instanceof Error) {
        logError(error) // Explicitly specify the type with type assertion
      }
    }
  }
}
