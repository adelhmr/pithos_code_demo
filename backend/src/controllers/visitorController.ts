import { isValidPassword } from './../utils/validationUtils'
import { type Request, type Response } from 'express'
import Visitor from '../models/visitorModels'
import jwt from 'jsonwebtoken'
import { passportParams } from '../../config/passport'
import { sendEmail, transporter } from '../services/emailService'
import { generateInitialIUPassword, generateToken, hashData } from '../utils/secUtils'
import { generateMagicLink } from '../utils/authenticationUtils'
import { EmailOptions } from '../interfaces/utilsAndServices/utilsInterfaces'

const authConfig = passportParams()

// Create New Visitor
export const registerVisitor = async (req: Request, res: Response): Promise<void> => {
  try {
    // Generate a random password for the Visitor
    const password = await generateInitialIUPassword()
    // Check if the email already exists
    const existingVisitor = await Visitor.findOne({ email: req.body.email })
    if (existingVisitor != null) {
      // 409 Status for data conflict
      res.status(409).json({ message: 'This email address is already in use' })
      return
    }

    // Create a new Visitor
    const visitor = new Visitor({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password, // Hash the password securely
      isActive: false, // Set the user as inactive initially
    })

    // Save the user to the database
    await visitor.save()

    const id = visitor._id
    const token = generateToken(
      { id, email: visitor.email },
      process.env.JWT_SECRET_KEY ?? '',
      '3h'
    )

    // Set the token as a cookie with a 15-minute expiry
    res.cookie('resetToken', token, {
      httpOnly: true,
      secure: false, // Use 'true' if using HTTPS
      sameSite: 'lax',
      maxAge: 900000, // Expiration time in milliseconds (e.g., 15 minutes)
    })
    // Create the magic link URL with the token
    const magicLink = generateMagicLink()
    // call send reset password mail
    const options = {
      senderEmail: 'contact@pithostechnology.com',
      receiverEmails: [visitor.email],
      subject: 'Reset password',
      text: 'This link is valid only for 1 hour',
      html: `<div>
        <p>To reset your password click to </p>
        <a href=${magicLink}>
         follow the link
        </a>
      </div>`,
    }

    await sendEmail(options)

    res.json({
      message: 'Internal user created successfully. Check your email for the magic link.',
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: 'internal server error',
    })
  }
  await Promise.resolve()
}

// Login Visitor
export const login = async (req: Request, res: Response): Promise<void> => {
  try {
    const { email, password } = req.body

    // Find the user in the database based on the provided email
    const visitor = await Visitor.findOne({ email })

    // If the user is not found or the password is incorrect, return an error response
    if (visitor == null || !visitor.comparePassword(password)) {
      res.status(401).json({ error: 'Invalid email or password' })
      return
    }

    // Generate a JWT token
    const token = jwt.sign({ id: visitor.id, email: visitor.email }, authConfig.jwtSecretKey, {
      expiresIn: authConfig.jwtExpiration,
    })

    // Return the token to the client
    res.status(200).json({ token })
  } catch (error) {
    console.error(error)
    res.status(500).json({ error: 'Internal server error' })
  }
}

export const profile = (req: Request, res: Response): void => {
  try {
    // Access the authenticated visitor from the request object
    const visitor = req.body.email

    // Return the user profile data
    res.status(200).json({ visitor })
  } catch (error) {
    console.error(error)
    res.status(500).json({ error: 'Internal server error' })
  }
}
// Get All users data
export const getAllVisitors = async (req: Request, res: Response): Promise<void> => {
  try {
    const visitors = await Visitor.find()
    res.json(visitors)
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
  }
}

// Get user By Id

// Update user By Id
export const updateVisitorById = async (req: Request, res: Response): Promise<void> => {
  const uId = req.params.id
  try {
    const visitor = await Visitor.findByIdAndUpdate(
      uId,
      { ...req.body, updatedAt: new Date() },
      { new: true }
    )
    if (visitor == null) {
      res.status(401).send('No such visitor')
      return
    }

    res.json(visitor)
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
  }
}
// Delete user By Id

export const deleteVisitorById = async (req: Request, res: Response): Promise<void> => {
  const uId = req.params.id
  try {
    const visitor = await Visitor.findByIdAndDelete(uId)
    if (visitor == null) {
      res.status(401).send('No such visitor')
      return
    }
    res.send('Visitor deleted successfully')
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
  }
}

// Delete All visitors
export const deleteAllVisitors = async (req: Request, res: Response): Promise<void> => {
  try {
    await Visitor.deleteMany({})
    res.send('All visitors deleted successfully')
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' })
  }
}

export const resetVisitor = async (req: Request, res: Response): Promise<void> => {
  try {
    const { email, password } = req.body

    if (!isValidPassword(password)) {
      res
        .status(401)
        .send('Password must include uppercase letters, lowercase letters, and numbers')
      return
    }
    const hashedPassword = await hashData(password, 10)

    const foundVisitor = await Visitor.findOne({ email })
    if (!foundVisitor) {
      res.status(401).send('User not found')
      return
    }
    foundVisitor.password = hashedPassword
    foundVisitor.updatedAt = new Date()
    await foundVisitor.save()
    res.redirect('/api/v2/login')
  } catch (error) {
    res.status(500).json({ error })
  }
}
