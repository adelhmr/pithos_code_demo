import { type Request, type Response, type NextFunction } from 'express'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import passport from 'passport'
import User from '../models/userModel'
import { passportParams } from '../../config/passport'
import passportJWT from 'passport-jwt'
import { type IUser } from '../interfaces/Api/IUserInterfaces'
import verifyRefreshToken from '../utils/verifyRefreshToken'

const ExtractJwt = passportJWT.ExtractJwt
const JwtStrategy = passportJWT.Strategy
const authConfig = passportParams()
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: authConfig.jwtSecretKey,
}

const jwtStrategy = new JwtStrategy(jwtOptions, (jwtPayload, done) => {
  // Retrieve the user from the database based on the user ID in the JWT payload
  User.findById(jwtPayload.id)
    .then((user) => {
      if (user == null) {
        done(null, false)
        return
      }

      // // Perform additional checks or validations based on the user schema
      // if (!user.isActive) {
      //   done(null, false);
      //   return;
      // }

      // if (user.role !== 'admin') {
      //   // Additional logic for non-admin users
      //   console.log('You are not allowed');
      // }

      // if (!user.email || user.email.length === 0 || user.email.trim() === '') {
      //   done(null, false);
      //   return;
      // }

      // // if (user.connection && user.connection.ipAddress) {
      // // const userIpAddress = jwtPayload.ip; // Assuming you include the IP address in the JWT payload
      // // if (!user.connection.ipAddress.includes(userIpAddress)) {
      // //   return done(null, false);
      // //  }
      // // }

      // // If all checks pass, construct the user object with relevant information
      const userWithAdditionalInfo = {
        id: user._id,
        email: user.email,
        role: user.role,
        // Add any additional information you want to include in the user object
      }

      done(null, userWithAdditionalInfo)
    })
    .catch((error) => {
      done(error, false)
    })
})

passport.use(jwtStrategy)

// export const authenticate = passport.authenticate('jwt', authConfig.jwtSession); // You can perform additional checks or database lookups here

export const authenticate = (req: Request, res: Response, next: NextFunction): void => {
  passport.authenticate('jwt', authConfig.jwtSession, async (err: Error, user: IUser) => {
    try {
      if (err) {
        throw err
      }

      if (!user) {
        res.status(401).json({ message: 'Unauthorized' })
        return
      }
      req.user = user
      const decodedToken = await verifyRefreshToken(user.id)
      if (decodedToken == null) {
        res.status(403).json({ message: 'User not authenticated' })
        return
      }

      const newAccessToken = jwt.sign(
        { id: decodedToken.id, email: decodedToken.email },
        authConfig.jwtSecretKey,
        {
          expiresIn: '5m',
        }
      )

      res.setHeader('Authorization', `Bearer ${newAccessToken}`)
    } catch (error) {
      console.error(error)
      res.status(500).json({ error: 'Internal server error' })
    }
    next()
  })(req, res, next)
}
