import React from 'react'
import { Link } from 'react-router-dom'

const Home: React.FC = () => {
  return (
    <div>
      <h1>Home Page</h1>
      <div className="bg-slate-600 ">
        <div>
          <Link to="/blog">Go to Blog</Link>
        </div>
        <div>
          <Link to="/analytics">Go to analytics</Link>
        </div>
      </div>
    </div>
  )
}

export default Home
