import { useState, useEffect } from 'react'
import useAuth from '../../hooks/useAuth'
import { useNavigate, useLocation } from 'react-router-dom'
import usersData from '../../api/auth/users.json'
import ReCAPTCHA from 'react-google-recaptcha'

/*
import dotenv from 'dotenv'

dotenv.config()
const recaptchSitekey: string = process.env.REACT_APP_RECAPTCHA_KEY ?? ''
if (recaptchSitekey === undefined || recaptchSitekey === '') {
  console.error('Missing configuration. Check your .env file.')
}
*/
/*
ReactDOM.render(
  <ReCAPTCHA 
     sitekey="Your client site key"
     onChange={onChange}
  />,
  document.body
)
*/
interface User {
  username: string
  password: string
  roles: string[]
}
export function getUserByUsername(username: string): User | null {
  const user = usersData.find((user) => user.username === username)
  return user !== undefined ? user : null
}
const Login = (): JSX.Element => {
  const { setAuth } = useAuth()

  const navigate = useNavigate()
  const location = useLocation()
  const path: string = location.state?.from?.pathname as string
  let from: string
  if (path.length > 0) {
    from = path
  } else {
    from = '/'
  }

  const [isRecaptchaCompleted, setIsRecaptchaCompleted] = useState(false) // Nouvel état pour le reCAPTCHA

  const [user, setUser] = useState('')
  const [pwd, setPwd] = useState('')
  const [errMsg, setErrMsg] = useState('')

  useEffect(() => {
    setErrMsg('')
  }, [user, pwd])

  const handleRecaptchaChange = (): void => {
    setIsRecaptchaCompleted(true) // Mettre à jour l'état du reCAPTCHA lorsqu'il est complété
  }

  /*
  if (!isRecaptchaCompleted) {
    setErrMsg('Please complete the reCAPTCHA') // Afficher un message d'erreur si le reCAPTCHA n'est pas complété
  }
  */
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault()

    // Fetch user details from the JSON file
    const userData = getUserByUsername(user)

    if (userData != null && userData.password === pwd) {
      // Login successful
      setAuth({ user, roles: userData.roles })
      setUser('')
      setPwd('')
      navigate(from, { replace: true })
    } else {
      // Login failed
      setErrMsg('Invalid username or password')
    }
  }

  return (
    <section>
      <p>{errMsg}</p>

      <form onSubmit={handleSubmit}>
        <div className="grid gap-6 mb-6 md:grid-cols-2">
          <div>
            <label
              htmlFor="first_name"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              username
            </label>
            <input
              type="text"
              id="first_name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="John"
              onChange={(e) => {
                setUser(e.target.value)
              }}
              autoComplete="off"
              value={user}
              required
            />
          </div>
        </div>
        <div className="grid gap-6 mb-6 md:grid-cols-2">
          <div>
            <label
              htmlFor="password"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Password
            </label>
            <input
              type="password"
              id="password"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="•••••••••"
              onChange={(e) => {
                setPwd(e.target.value)
              }}
              value={pwd}
              required
            />
          </div>
        </div>
        <ReCAPTCHA
          sitekey="6LcgLHInAAAAAGZUUlGv5St5bg1SHlW8bhcuuYgB"
          onChange={handleRecaptchaChange} // Mettre à jour l'état du reCAPTCHA lorsqu'il est complété
        />
        <button
          type="submit"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          disabled={!isRecaptchaCompleted}
        >
          Submit
        </button>
      </form>
    </section>
  )
}

export default Login
