import { Outlet } from 'react-router-dom'
import NavBar from '../../components/layout/navBar/NavBar'
import { SideNav } from '../../components/layout/sideNav/SideNav'

const Layout: React.FC = () => {
  return (
    <>
      <NavBar />
      <div className="flex flex-row h-full">
        <SideNav />
        <Outlet />
      </div>
    </>
  )
}

export default Layout
