import { FiBell } from 'react-icons/fi'

interface NotificationButtonProps {
  notificationCount: number
}

const NotificationButton: React.FC<NotificationButtonProps> = ({
  notificationCount = 0,
}: NotificationButtonProps) => {
  return (
    <button className="relative p-2 mx-1 bg-transparent rounded-sm dark:text-white text-gray-500 hover:bg-gray-200 focus:outline-none z-100 dark:bg-transparent">
      <FiBell size={18} />
      {notificationCount > 0 && (
        <span className="absolute top-0 right-0 h-4 w-4 bg-red-500 rounded-full flex items-center justify-center text-xs text-white">
          {notificationCount}
        </span>
      )}
    </button>
  )
}

export default NotificationButton
