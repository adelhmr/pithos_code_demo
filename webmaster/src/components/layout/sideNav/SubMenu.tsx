// SubMenu.tsx

import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { BsChevronDown, BsChevronUp } from 'react-icons/bs'

// Define a type or interface for the sidebar item
export interface SidebarItem {
  title: string
  path: string
  icon: JSX.Element // Assuming the icon is a JSX element
  subNav?: SidebarItem[] // Optional sub-menu items of the same type
  iconOpened?: JSX.Element // Define icon for opened state
  iconClosed?: JSX.Element // Define icon for closed state
  isOpen: boolean
}

const SubMenu: React.FC<{ item: SidebarItem }> = ({ item }) => {
  const [subnav, setSubnav] = useState(item.isOpen || false)

  const showSubnav = (): void => {
    setSubnav(!subnav)
  }

  // Define the icons for opened and closed states of sub-menu
  const iconOpened = <BsChevronUp />
  const iconClosed = <BsChevronDown />
  /*
  const handleMainItemClick = () : void => {
    if (item.subNav != null && item.subNav.length > 0) {
      showSubnav()
    }
  }
*/
  return (
    <>
      <Link
        to={item.path}
        onClick={() => {
          if (item.subNav !== undefined && item.subNav.length > 0) {
            showSubnav()
          }
        }}
        className="w-full flex text-white justify-between items-center py-3 px-2 text-lg text-left hover:bg-gray-700 border-b border-white"
        style={{ paddingLeft: '20px' }} // Add padding to create space on the left
      >
        <div className="flex items-center">
          <span className="dark:text-orange-500 text-dark hover:text-white">
            {item.icon}
          </span>
          <span
            className={`ml-2 text-sm dark:text-orange-500 ${
              item.isOpen ? 'hidden' : ''
            }`}
          >
            {item.title}
          </span>{' '}
          {/* Use text-sm for smaller font size */}
        </div>
        <div>
          {item.subNav != null && (subnav ? item.iconOpened : item.iconClosed)}
        </div>
        <div>{item.subNav != null && (subnav ? iconOpened : iconClosed)}</div>
      </Link>
      {subnav &&
        item.subNav?.map((subItem, index) => {
          return (
            <Link
              to={subItem.path}
              key={index}
              className="flex items-center py-3 px-12 text-base text-left hover:bg-gray-700 border-b border-white w-full"
              style={{ paddingLeft: '40px' }} // Add more padding to create space on the left
            >
              <span className="overflow-hidden dark:text-orange-500 text-dark hover:text-white">
                {subItem.icon}
              </span>
              <span
                className={`ml-2 text-sm dark:text-orange-500 hover:text-white ${
                  item.isOpen ? 'hidden' : ''
                }`}
              >
                {subItem.title}
              </span>
            </Link>
          )
        })}
    </>
  )
}

export default SubMenu
