import { FiMail } from 'react-icons/fi'

interface MessageButtonProps {
  messageCount: number
}

const MessageButton: React.FC<MessageButtonProps> = ({
  messageCount = 0,
}: MessageButtonProps) => {
  return (
    <button className="relative p-2 mx-1 z-50 bg-transparent dark:text-white rounded-sm text-gray-800 hover:bg-gray-200 focus:outline-none dark:bg-transparent">
      <FiMail size={18} />
      {messageCount > 0 && (
        <span className="absolute top-0 right-0 h-4 w-4 bg-red-500 rounded-full flex items-center justify-center text-xs text-white">
          {messageCount}
        </span>
      )}
    </button>
  )
}

export default MessageButton
