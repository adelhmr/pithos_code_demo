import React, { useState } from 'react'
import {
  BsChevronLeft,
  BsChevronRight,
  BsHouseDoor,
  BsBox,
} from 'react-icons/bs'
import { FaProductHunt } from 'react-icons/fa'
import SubMenu, { type SidebarItem } from './SubMenu'

// Define your sidebar items data
const sidebarData: SidebarItem[] = [
  {
    title: 'Home',
    path: '/',
    icon: <BsHouseDoor />, // Use BsHouseDoor icon for Home
    isOpen: true,
  },
  {
    title: 'Dashboard',
    path: '/',
    icon: <BsBox />, // Use BsBox icon for Products
    isOpen: true,
    subNav: [
      {
        title: 'Blog',
        isOpen: true,
        path: '/blog',
        icon: <FaProductHunt />,
      },

      {
        title: 'Analytics',
        isOpen: true,
        path: '/analytics',
        icon: <FaProductHunt />,
      },
      // Add more sub-menu items as needed
    ],
  },
  // Add more sidebar items as needed
]

export const SideNav: React.FC = () => {
  const [isToggled, setToggled] = useState(false)
  const handleToggle = (): void => {
    setToggled(!isToggled)
  }
  // Set isOpen for "Products" and "Home" to true when isToggled is true
  sidebarData[0].isOpen = isToggled // "Home"
  sidebarData[1].isOpen = isToggled // "Products"
  return (
    <div
      className={`relative text-2xl text-center overflow-hidden dark:text-white dark:bg-gray-900 dark:border-gray-500 border-orange-500 border-r-[1px] px-3  ${
        isToggled ? 'w-[8rem]' : 'w-[18rem]'
      }`}
    >
      <button
        className={`
        font-normal
        absolute
        cursor-pointer
        w-15
        right-0
        top-8
        border-[1px]
        border-r-0
        rounded-l-full
        p-2
        text-orange-500
        hover:bg-orange-200
        dark:border-gray-500
        border-orange-500
        dark:bg-orange-100/75
        dark:hover:border-orange-500 shadow-inner
      `}
        onClick={handleToggle}
      >
        {isToggled ? (
          <span className="hover:rotate-180 transition duration-700 ease-in-out hover:font-extrabold hover:shadow-lg ">
            <BsChevronRight />
          </span>
        ) : (
          <span className="hover:rotate-180 transition duration-700 ease-in-out hover:font-extrabold hover:shadow-lg ">
            <BsChevronLeft />
          </span>
        )}
      </button>
      <section className="overflow-hidden h-full">
        MENU
        <div
          className={`flex flex-col items-center w-full ${
            isToggled ? 'my-3 dark:text-orange-500 text-dark' : ''
          }`}
        >
          {sidebarData.map((item, index) => {
            // Show only icons if isToggled is true

            // Render the SubMenu component if isToggled is false
            return <SubMenu key={index} item={item} />
          })}
        </div>
      </section>
    </div>
  )
}
