import { Link, Outlet } from 'react-router-dom'

export default function AuthLayout(): JSX.Element {
  return (
    <>
      <nav className="flex flex-col bg-slate-700">
        <ul>
          <li>
            <Link to="home"> Home</Link>
          </li>
          <li>
            <Link to="blog"> blog</Link>
          </li>
          <li>
            <Link to="/analytics"> analytics</Link>
          </li>
        </ul>
      </nav>
      <Outlet />
    </>
  )
}
