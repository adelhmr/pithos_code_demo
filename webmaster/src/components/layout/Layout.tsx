import { useTranslation } from 'react-i18next'
import User from '../home/User'
import Translator from './navBar/Translator'
import Switcher from './navBar/themeSwitcher'
import React, { useEffect, useState } from 'react'
import NavBar from './navBar/NavBar'
import { type Payment, columns } from '../../components/table/columns'
import { DataTable } from '../../components/table/data-table'

async function getData(): Promise<Payment[]> {
  // Fetch data from your API here.
  return [
    {
      id: 'm5gr84i9',
      amount: 316,
      status: 'success',
      email: 'ken99@yahoo.com',
    },
    {
      id: '3u1reuv4',
      amount: 242,
      status: 'success',
      email: 'Abe45@gmail.com',
    },
    {
      id: 'derv1ws0',
      amount: 837,
      status: 'processing',
      email: 'Monserrat44@gmail.com',
    },
    {
      id: '5kma53ae',
      amount: 874,
      status: 'success',
      email: 'Silas22@gmail.com',
    },
    {
      id: 'bhqecj4p',
      amount: 721,
      status: 'failed',
      email: 'carmella@hotmail.com',
    },
  ]
}

const Layout: React.FC = () => {
  const { t } = useTranslation()
  const [data, setData] = useState<Payment[]>([])

  // Specify the return type of the async function as void
  useEffect(() => {
    async function fetchData(): Promise<void> {
      try {
        const fetchedData = await getData()
        setData(fetchedData)
      } catch (error) {
        // Handle error, e.g., log or display an error message.
        console.error('Error fetching data:', error)
      }
    }

    // Call fetchData() inside useEffect and handle the promise
    fetchData().catch((error) => {
      // Handle error from the fetchData() function itself.
      console.error('Error in fetchData():', error)
    })
  }, []) // Empty dependency array to ensure it runs only once on mount
  const customButtons = [
    {
      label: 'Button 1',
      onClick: () => {
        console.log('Custom Button 1 clicked')
      },
      className: ' hover:bg-blue-700 dark:bg-red-900 ',
    },
    {
      label: 'Button 2',
      onClick: () => {
        console.log('Custom Button 2 clicked')
      },
    },
    // Add more custom buttons as needed
  ]
  return (
    <>
      <NavBar />

      <Switcher />
      <Translator />

      <div className=" text-blue-500 text-2xl   text-center dark:text-white  dark:bg-black">
        <p>
          <h3>{t('Thanks.1')}</h3> <h3>{t('Why.1')}</h3>
        </p>
        <User />
        <DataTable
          columns={columns}
          data={data}
          showFooter={true}
          showSearch={true}
          buttons={customButtons}
          Dropdown={true}
        />
      </div>
    </>
  )
}

export default Layout
