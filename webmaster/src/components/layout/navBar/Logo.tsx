import pgtLogo from '../../../assets/logoPgt.png'

const Logo: React.FC = () => {
  return (
    <>
      <div>
        <img
          src={pgtLogo}
          alt="Pithos Global Technology"
          width="80"
          height="40"
        />
      </div>
    </>
  )
}

export default Logo
