import { useEffect, useState } from 'react'
import { BsFillMoonStarsFill, BsFillSunFill, BsLaptop } from 'react-icons/bs'
export default function Switcher(): React.ReactNode {
  const [theme, setTheme] = useState(localStorage.getItem('theme') ?? 'system')
  const element = document.documentElement
  const darkQuery = window.matchMedia('(prefers-color-scheme: dark)')

  const options = [
    {
      icon: <BsFillSunFill />,
      text: 'light',
    },
    {
      icon: <BsFillMoonStarsFill />,
      text: 'dark',
    },
    {
      icon: <BsLaptop />,
      text: 'system',
    },
  ]
  function onWindowMatch(): void {
    if (
      localStorage.theme === 'dark' ||
      (!('theme' in localStorage) && darkQuery.matches)
    ) {
      element.classList.add('dark')
    } else {
      element.classList.remove('dark')
    }
  }
  onWindowMatch()
  useEffect(() => {
    switch (theme) {
      case 'dark':
        element.classList.add('dark')
        localStorage.setItem('theme', 'dark')
        break
      case 'light':
        element.classList.remove('dark')
        localStorage.setItem('theme', 'light')

        break

      default:
        localStorage.removeItem('theme')
        onWindowMatch()
        break
    }
  }, [theme])
  darkQuery.addEventListener('change', (e) => {
    if (!('theme' in localStorage)) {
      if (e.matches) {
        element.classList.add('dark')
      } else {
        element.classList.remove('dark')
      }
    }
  })
  return (
    <>
      <div
        className="
        flex
        flex-row
        py-1
        align-center
        h-8
        justify-between
        border
      border-gray-300
      dark:border-gray-600
        mx-2
        rounded-full
        px-2
        dark:hover:border-orange-500
        hover:border-orange-500
        hover:ring-orange-500
        "
      >
        {options.map((opt) => (
          <button
            key={opt.text}
            onClick={() => {
              setTheme(opt.text)
            }}
            className={` w-6 h-6 text-xl rounded-full justify-center align-center bg-transparent text-center flex mx-1
          ${theme === opt.text ? 'text-yellow-300 ' : 'text-gray-400'} `}
          >
            {opt.icon}
          </button>
        ))}
      </div>
    </>
  )
}
