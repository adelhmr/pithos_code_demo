import Translator from './Translator'
import Container from '../../shared/container'
import Switcher from './themeSwitcher'
import Avatar from './Avatar'
import Logo from './Logo'
import SearchBar from './SearchBar'
import NotificationButton from '../sideNav/NotifBtn'
import MessageButton from '../sideNav/MessageBtn'

const NavBar: React.FC = () => {
  return (
    <>
      <div className="w-full bg-white dark:bg-gray-900 shadow-sm">
        <div className="py-2 border-b-[1px] border-orange-500 dark:border-gray-700 text-center">
          <Container>
            <div className="flex flex-row justify-between items-center">
              <Logo />
              <div className="flex w-[30%]">
                <SearchBar />
              </div>
              <div className="flex flex-row justify-between items-center">
                <div className="flex flex-row items-center justify-around mr-2">
                  <span className="relative z-30">
                    <MessageButton messageCount={5} />
                  </span>
                  <span className="relative z-30">
                    <NotificationButton notificationCount={2} />
                  </span>
                </div>
                <div className="flex justify-center">
                  <Translator />
                  <Switcher />
                </div>
                <Avatar />
              </div>
            </div>
          </Container>
        </div>
      </div>
    </>
  )
}

export default NavBar
