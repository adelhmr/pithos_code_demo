import { BiSearch } from 'react-icons/bi'

const Search: React.FC = () => {
  return (
    <div className="relative focus:outline-none w-full p-3">
      <input
        type="search"
        name="gSearch"
        id="webmasterSearch"
        placeholder="Search...."
        className="
          focus:ring-transparent
        focus:outline-orange-500
        border-gray-300
        dark:border-gray-600
          border-[1px] 
          w-full
          py-2 
          px-4
          z-2
          rounded-full 
          bg-transparent
          transition 
          cursor-pointer
          flex
          focus:border-none
        "
      />
      <div
        className="
          p-2 
          bg-orange-500
          rounded-full 
          text-white
          absolute
          z-15
          right-4
          bottom-4
          focus:outline-none
        "
      >
        <BiSearch size={18} />
      </div>
    </div>
  )
}

export default Search
