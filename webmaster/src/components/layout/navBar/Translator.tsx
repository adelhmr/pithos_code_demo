import i18next from 'i18next'
import { useState, useEffect } from 'react'

const Translator: React.FC = () => {
  const [TranslationLanguage, setTranslationLanguage] = useState('')

  useEffect(() => {
    const storedLanguage = localStorage.getItem('translationLanguage')
    const languageToSet = storedLanguage ?? 'en'
    setTranslationLanguage(languageToSet)
    i18next.changeLanguage(TranslationLanguage).catch((error) => {
      console.error('Error initializing i18next:', error)
    })
  }, [TranslationLanguage])
  function handleClick(lang: string): void {
    setTranslationLanguage(lang)
    localStorage.setItem('translationLanguage', lang)
  }

  return (
    <div>
      <select
        className=" bg-gray-50 border text-center focus:dark:ring-none border-gray-300 h-8 w-13 text-gray-900 text-xs rounded-lg focus:ring-gray-500 focus:border-gray-500  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-orange-500 dark:focus:border-orange-500"
        value={TranslationLanguage}
        onChange={(e) => {
          handleClick(e.target.value)
        }}
      >
        <option value="en">EN</option>
        <option value="fr">FR</option>
        <option value="ar">AR</option>
      </select>
    </div>
  )
}

export default Translator
