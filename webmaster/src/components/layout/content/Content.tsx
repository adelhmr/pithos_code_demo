import Container from '../../shared/container'
import { useTranslation } from 'react-i18next'
import User from '../../home/User'
import { SideNav } from '../sideNav/SideNav'

const Content: React.FC = () => {
  const { t } = useTranslation()
  return (
    <div className=" h-full w-auto flex flex-row z-0">
      <div className="relative z-5">
        <SideNav />
      </div>
      <Container>
        <div className="relative text-2xl   text-center dark:text-white  dark:bg-gray-900">
          <p>
            <h3>{t('Thanks.1')}</h3> <h3>{t('Why.1')}</h3>
          </p>
          <User />
        </div>
        <p className="lead">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor sed
          itaque cumque aliquid dicta ullam id quis, dolorem assumenda ipsam,
          quae voluptate quos officiis quibusdam quasi odio aliquam numquam
          voluptas? Laborum placeat, sit adipisci at cupiditate quae id beatae?
          Quasi veritatis incidunt explicabo, laboriosam saepe reiciendis harum
          quos mollitia labore fugiat rerum doloribus quo placeat. Ea alias
          culpa inventore quos laborum eligendi dolorum, magnam sunt in fugiat
          recusandae deleniti adipisci vero cum perspiciatis delectus odit ipsum
          assumenda obcaecati magni quisquam, eius ullam temporibus commodi!
          Accusantium alias mollitia sed, saepe quis maxime temporibus,
          necessitatibus consequuntur odio doloremque veritatis sunt velit quam
          beatae, eligendi atque at impedit autem corporis inventore vero.
          Tenetur, quas amet doloremque enim, minima ut alias nemo consequuntur,
          officiis fugit aperiam corrupti dolorum ea odit laborum suscipit qui
          iste iure possimus voluptatem nisi quod perspiciatis molestias?
          Aspernatur nisi ad id odio amet consequatur adipisci tempora similique
          iusto, suscipit voluptas, sit quibusdam? Perspiciatis sunt quisquam
          facere earum illum incidunt quae similique nihil. Magni, ullam atque,
          eligendi ipsum minus similique voluptatum eius deserunt, maiores
          repellendus fuga beatae explicabo ea libero nam sequi quam vero
          exercitationem voluptate accusantium? Distinctio quo laudantium, alias
          mollitia corrupti quae aperiam temporibus cumque, porro odio labore
          eveniet.
        </p>
      </Container>
    </div>
  )
}

export default Content
