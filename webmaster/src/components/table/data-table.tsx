'use client'

import {
  type ColumnDef,
  type SortingState,
  flexRender,
  getCoreRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
  getFilteredRowModel,
  type ColumnFiltersState,
  type VisibilityState,
  type Column,
} from '@tanstack/react-table'
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from '../ui/dropdown-menu'

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '../ui/table'
import { Button } from '../ui/button'
import React, { useMemo, useState } from 'react'
import { Input } from '../ui/input'
import { Checkbox } from '../ui/checkbox'

interface DataTableProps<TData, TValue> {
  columns: Array<ColumnDef<TData, TValue>>
  data: TData[]
  showFooter?: boolean
  showSearch?: boolean
  buttons?: Array<{ label: string; onClick: () => void; className?: string }>
  Dropdown?: boolean
  showColums?: boolean
}

export function DataTable<TData, TValue>({
  columns,
  data,
  showFooter = true,
  showSearch = true,
  buttons,
  Dropdown = true,
}: DataTableProps<TData, TValue>): JSX.Element {
  const [sorting, setSorting] = React.useState<SortingState>([])
  const [columnFilters, setColumnFilters] = React.useState<ColumnFiltersState>(
    [],
  )
  const [columnVisibility, setColumnVisibility] =
    React.useState<VisibilityState>({})
  const [rowSelection, setRowSelection] = React.useState<
    Record<number, boolean>
  >({}) // Specify the type for rowSelection

  const table = useReactTable({
    data,
    columns,
    onSortingChange: setSorting,
    onColumnFiltersChange: setColumnFilters,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onColumnVisibilityChange: setColumnVisibility,
    onRowSelectionChange: setRowSelection,

    state: {
      sorting,
      columnFilters,
      columnVisibility,
      rowSelection,
    },
  })

  const [selectedColumns, setSelectedColumns] = useState<string[]>([])
  const [searchValue, setSearchValue] = useState<string>('')

  const handleCheckboxChange = (column: Column<TData, unknown>): void => {
    setSelectedColumns((prevSelectedColumns) => {
      const columnId = column.id
      if (prevSelectedColumns.includes(columnId)) {
        // If the column is already in the selected columns, remove it
        return prevSelectedColumns.filter((colId) => colId !== columnId)
      } else {
        // Otherwise, add the column to the selected columns
        return [...prevSelectedColumns, columnId]
      }
    })
  }

  const filteredData = useMemo(() => {
    if (selectedColumns.length === 0) {
      // If no columns are selected and there's no search value, return the original data
      if (searchValue.length === 0) {
        return data
      } else {
        // Apply the search filter to all columns in the table
        return data.filter((row) => {
          for (const column of columns) {
            const cellValue =
              'accessorKey' in column
                ? (row as Record<string, unknown>)[column.accessorKey as string]
                : undefined
            if (
              cellValue
                ?.toString()
                .toLowerCase()
                .includes(searchValue.toLowerCase()) ??
              false
            ) {
              return true // Return true if the search value matches the cell value in any column
            }
          }
          return false // Return false if the search value doesn't match any cell value in the table
        })
      }
    } else {
      // Apply the search filter only to the selected columns
      return data.filter((row) => {
        for (const columnId of selectedColumns) {
          const cellValue = (row as Record<string, unknown>)[columnId]
          if (
            cellValue
              ?.toString()
              .toLowerCase()
              .includes(searchValue.toLowerCase()) ??
            false
          ) {
            return true // Return true if the search value matches the cell value
          }
        }
        return false // Return false if the search value doesn't match any selected column's cell value
      })
    }
  }, [data, columns, selectedColumns, searchValue])

  const handleSearchFilterChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    setSearchValue(event.target.value)
  }

  return (
    <div className="ml-4">
      <div className="flex items-center py-4 ">
        {showSearch ? (
          <>
            <DropdownMenu>
              <DropdownMenuTrigger asChild>
                <Button variant="outline">
                  {selectedColumns.length === 0
                    ? 'all'
                    : selectedColumns.length === 1
                    ? selectedColumns[0] // Show the name of the selected column if only one is selected
                    : `${selectedColumns.length} selected`}
                </Button>
              </DropdownMenuTrigger>
              <DropdownMenuContent className="auto">
                {table
                  .getAllColumns()
                  .filter((column) => column.getCanFilter())
                  .map((column) => (
                    <div key={column.id}>
                      <Checkbox
                        id={column.id}
                        checked={selectedColumns.includes(column.id)}
                        onClick={() => {
                          handleCheckboxChange(column)
                        }}
                      />
                      <label className="text-center ml-2" htmlFor={column.id}>
                        {column.id}
                      </label>
                    </div>
                  ))}
              </DropdownMenuContent>
            </DropdownMenu>

            <Input
              placeholder={
                selectedColumns.length === 0
                  ? 'Filter all table ...'
                  : selectedColumns.length === 1
                  ? `Filter by ${selectedColumns[0]}` // Show the name of the selected column if only one is selected
                  : `Filter by ${selectedColumns.join(', ')}` // Show the names of the selected columns if more than one column is selected
              }
              value={searchValue}
              onChange={handleSearchFilterChange}
              className="max-w-sm"
            />
          </>
        ) : null}
        {Dropdown ? (
          <DropdownMenu>
            <DropdownMenuTrigger asChild>
              <Button variant="outline" className="ml-auto">
                Columns
              </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent align="end">
              {table
                .getAllColumns()
                .filter((column) => column.getCanHide())
                .map((column) => (
                  <DropdownMenuCheckboxItem
                    key={column.id}
                    className="capitalize"
                    checked={column.getIsVisible()}
                    onCheckedChange={(value) => {
                      column.toggleVisibility(!!value)
                    }}
                  >
                    {column.id}
                  </DropdownMenuCheckboxItem>
                ))}
            </DropdownMenuContent>
          </DropdownMenu>
        ) : null}
        {buttons?.map((button, index) => (
          <Button
            key={index}
            onClick={button.onClick}
            className={button.className}
          >
            {button.label}
          </Button>
        ))}
      </div>

      <div className="rounded-md border">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => (
                  <TableHead key={header.id}>
                    {header.isPlaceholder
                      ? null
                      : flexRender(
                          header.column.columnDef.header,
                          header.getContext(),
                        )}
                  </TableHead>
                ))}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows.map((row) => {
              const isRowVisible = filteredData.includes(row.original)
              if (isRowVisible) {
                return (
                  <TableRow
                    key={row.id}
                    data-state={row.getIsSelected() ? 'selected' : undefined}
                  >
                    {row.getVisibleCells().map((cell) => (
                      <TableCell key={cell.id}>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext(),
                        )}
                      </TableCell>
                    ))}
                  </TableRow>
                )
              } else {
                // Render an empty fragment for rows that are not in filteredData
                return <React.Fragment key={row.id}></React.Fragment>
              }
            })}
          </TableBody>
        </Table>
      </div>
      {showFooter ? (
        <div className="flex items-center justify-end space-x-2 py-4">
          <div className="flex-1 text-sm text-muted-foreground">
            {table.getFilteredSelectedRowModel().rows.length} of{' '}
            {table.getFilteredRowModel().rows.length} row(s) selected.
          </div>
          <Button
            variant="outline"
            size="sm"
            onClick={() => {
              table.previousPage()
            }}
            disabled={!table.getCanPreviousPage()}
          >
            Previous
          </Button>
          <Button
            variant="outline"
            size="sm"
            onClick={() => {
              table.nextPage()
            }}
            disabled={!table.getCanNextPage()}
          >
            Next
          </Button>
        </div>
      ) : null}
    </div>
  )
}
