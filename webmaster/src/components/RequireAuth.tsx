import { useLocation, Navigate, Outlet } from 'react-router-dom'
import useAuth from '../hooks/useAuth'

interface RequireAuthProps {
  allowedRoles: string[]
}

const RequireAuth: React.FC<RequireAuthProps> = ({
  allowedRoles,
}: RequireAuthProps) => {
  const { auth } = useAuth()
  const location = useLocation()
  const roles = auth?.roles ?? []
  const user = auth?.user ?? null
  console.log(user)
  const hasAllowedRole = roles.find((role) => allowedRoles?.includes(role))

  return hasAllowedRole != null ? (
    <Outlet />
  ) : user !== null ? (
    <Navigate to="/unauthorized" state={{ from: location }} replace />
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  )
}

export default RequireAuth
