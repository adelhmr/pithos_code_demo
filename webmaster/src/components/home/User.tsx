import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { type RootState } from '../../redux/store/configureStore'
import {
  setName,
  setAge,
  resetUser,
} from '../../redux/actions/users/userActionsCreators'

const UserComponent: React.FC = () => {
  const user = useSelector((state: RootState) => state.user)
  const dispatch = useDispatch()

  const handleSetName = (): void => {
    dispatch(setName('John Doe'))
  }

  const handleSetAge = (): void => {
    dispatch(setAge(25))
  }

  const handleResetUser = (): void => {
    dispatch(resetUser())
  }

  return (
    <div>
      <h1>User Info</h1>
      <p>Name: {user.name}</p>
      <p>Age: {user.age}</p>
      <button onClick={handleSetName}>Set Name</button>
      <button onClick={handleSetAge}>Set Age</button>
      <button onClick={handleResetUser}>Reset User</button>
    </div>
  )
}

export default UserComponent
