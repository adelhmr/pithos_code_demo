import { useRoutes } from 'react-router-dom'
import SignIn from '../views/signIn/Signin'
import Blog from '../views/blog/Blog'
import Home from '../views/Home/home'

import Layout from '../views/layout/Layout'
import Unauthorized from '../views/unauthorized/Unauthorized'
import Analytics from '../views/analytics/Analytics'
import RequireAuth from './RequireAuth'

const Router: React.FC = (): React.ReactNode => {
  const routes = useRoutes([
    {
      path: '/',
      element: <RequireAuth allowedRoles={['user']} />,
      children: [
        {
          path: '/',
          element: <Layout />,
          children: [
            {
              index: true,
              path: '/',
              element: <Home />,
            },
          ],
        },
      ],
    },
    {
      path: '/',
      element: <RequireAuth allowedRoles={['admin']} />,
      children: [
        {
          path: '/',
          element: <Layout />,
          children: [
            {
              path: '/blog',
              element: <Blog />,
            },
            {
              path: '/analytics',
              element: <Analytics />,
              children: [{}],
            },
          ],
        },
      ],
    },
    {
      path: '/login',
      element: <SignIn />,
    },
    {
      path: '/unauthorized',
      element: <Unauthorized />,
    },
  ])

  return routes
}
export default Router
