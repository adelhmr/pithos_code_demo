import { useContext } from 'react'
import AuthContext from '../context/AuthProvider'

interface AuthContextType {
  auth: { user: string; roles: string[] } // Replace with the actual type of 'auth'
  setAuth: React.Dispatch<
    React.SetStateAction<{ user: string; roles: string[] }>
  > // Replace with the actual type of 'setAuth'
}

// useAuth hook with proper return type
const useAuth = (): AuthContextType => {
  const authContextValue = useContext(AuthContext) as AuthContextType
  return authContextValue
}

export default useAuth
