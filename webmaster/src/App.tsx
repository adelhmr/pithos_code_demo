import Router from './components/Router'

function App(): JSX.Element {
  return (
    <div className=" h-screen w-screen bg-white dark:bg-gray-900 ">
      <Router />
    </div>
  )
  
}
//      <Layout/>
//      <Layout/>

export default App
