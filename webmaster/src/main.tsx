import React, { Suspense } from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom/client'
import store from './redux/store/configureStore.ts'
import App from './App.tsx'
import './index.css'
import './internationalization/internationalization.ts'
import { BrowserRouter } from 'react-router-dom'
import { AuthProvider } from './context/AuthProvider.tsx'
/*
import dotenv from 'dotenv'

dotenv.config()
*/
const rootElement = document.getElementById('root')

if (rootElement != null) {
  ReactDOM.createRoot(rootElement).render(
    <React.StrictMode>
      <Suspense fallback={<div>Loading ...</div>}>
        <Provider store={store}>
          <AuthProvider>
            <BrowserRouter>
              <App />
            </BrowserRouter>
          </AuthProvider>
        </Provider>
      </Suspense>
    </React.StrictMode>,
  )
}
