import { SET_NAME, SET_AGE, RESET_USER } from './userActionTypes'

export const setName = (name: string): { type: string; payload: string } => ({
  type: SET_NAME,
  payload: name,
})

export const setAge = (age: number): { type: string; payload: number } => ({
  type: SET_AGE,
  payload: age,
})




export const resetUser = (): { type: string } => ({
  type: RESET_USER,
})
