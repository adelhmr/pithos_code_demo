import { SET_NAME, SET_AGE, RESET_USER } from '../actions/users/userActionTypes'

export interface UserState {
  // Define the shape of the user state
  name: string
  age: number
}

interface SetNameAction {
  type: 'SET_NAME'
  payload: string
}

interface SetAgeAction {
  type: 'SET_AGE'
  payload: number
}

interface ResetUserAction {
  type: 'RESET_USER'
}

export type UserAction = SetNameAction | SetAgeAction | ResetUserAction

const initialState: UserState = {
  name: '',
  age: 0,
}

const userReducer = (
  state: UserState = initialState,
  action: UserAction,
): UserState => {
  switch (action.type) {
    case SET_NAME:
      return {
        ...state,
        name: action.payload,
      }
    case SET_AGE:
      return {
        ...state,
        age: action.payload,
      }
    case RESET_USER:
      return initialState
    default:
      return state
  }
}

export default userReducer
