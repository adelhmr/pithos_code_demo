import { combineReducers, type Reducer } from 'redux'
import userReducer, { type UserState } from './userReducer'
// import cartReducer, { CartState } from './cartReducer';
// import productReducer, { ProductState } from './productReducer';

interface RootState {
  user: UserState
  // cart: CartState;
  // product: ProductState;
}

const rootReducer: Reducer<RootState> = combineReducers<RootState>({
  user: userReducer,
  // cart: cartReducer,
  // product: productReducer,
})

export default rootReducer
