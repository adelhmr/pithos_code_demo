import { type PropsWithChildren, createContext, useState } from 'react'

const AuthContext = createContext({})

export const AuthProvider = ({
  children,
}: PropsWithChildren<Record<string, unknown>>): JSX.Element => {
  const [auth, setAuth] = useState({})

  return (
    <AuthContext.Provider value={{ auth, setAuth }}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthContext
